﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class declinseController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.url = "https://travelrewardsvacations.com/special#section1";
            if (Session["cell"] == null)
            {
                return RedirectToAction("index", "main");
            }
            string card = Session["number1"] != null ? Session["number1"].ToString() : "";
            var view = "Index";
            var status = "Decline";
            string phone = Session["cell"].ToString();
            updatelead(status, cardnumber: card);
            if (TempData["error"] != null)
            {
                ViewBag.error = TempData["error"].ToString();
            }
            try
            {
                using (var client = new WebClient())
                {
                    var values = new NameValueCollection();
                    values["phone"] = Session["cell"].ToString();
                    values["email"] = Session["email"].ToString();
                    values["type"] = "decline";
                    var response2 = client.UploadValues("https://getultimateoffer.com/sms/updatefblead", values);
                    var responseString1 = System.Text.Encoding.Default.GetString(response2);
                }
            }
            catch (Exception ex)
            {

            }

            return View(view);
        }

        public void updatelead(string status = "Decline", string cardnumber = "", string phone = "")
        {
            int lead = Convert.ToInt32(Session["id"]);
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@status", status);
                if (!string.IsNullOrEmpty(phone))
                {
                    _srtlist.Add("@phone", phone);
                }
                _srtlist.Add("@userId", lead);
                _srtlist.Add("@cardnumber", cardnumber);
                _dt.executeNonQuery("UpdateStatus", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }

    }
}