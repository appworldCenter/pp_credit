﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;
using WebApplication1.Models;
using WebApplication1.utility;

namespace WebApplication1.Controllers
{
    public class v1Controller : ApiController
    {
        public class url
        { 
            public string _url { get; set; }
        }
         

        // vsa.gifts
        [HttpGet]
        public HttpResponseMessage shorten(string longUrl, string name)
        {
            url _model = new url();
            _model._url = getnewurl(longUrl, name);
            return Request.CreateResponse(HttpStatusCode.OK, _model); 
        }
        
        [HttpPost]
        public HttpResponseMessage shortenPost(string longUrl, string name)
        {
            url _model = new url();
            _model._url = getnewurl(longUrl, name);
            return Request.CreateResponse(HttpStatusCode.OK, _model);
        }

        public string getnewurl(string longurl, string name)
        {
            string message = longurl;
            SqlHelperold sqlHelper = new SqlHelperold();
            try
            {
                SortedList _srt = new SortedList(); 
                _srt.Add("@longurl", longurl);
                _srt.Add("@name", name);
                message = sqlHelper.executeNonQueryWMessage("Addurl","", _srt).ToString();

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = longurl;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }




    }
}
