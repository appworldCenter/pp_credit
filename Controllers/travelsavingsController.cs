﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class travelsavingsController : Controller
    {  // GET: travel_savings
        public ActionResult Index()
        {
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                return Redirect(redirectUrl);
            }
            formmodal _model = new formmodal();
            Session.Abandon();
            return View(_model);
        }
    }
}