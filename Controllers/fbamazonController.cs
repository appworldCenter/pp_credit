﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class fbamazonController : Controller
    {
        public ActionResult Checkout()
        {
            Session["url"] = "https://travelrewardsvacations.com/special/fbamazon/checkout";
            if (Session["firstname"] == null)
            {
                return RedirectToAction("index", "main");
            }
            try
            {
                formmodal model = new Models.formmodal();
           
            model.firstname = Session["firstname"].ToString();
            model.lastname = Session["lastname"].ToString();
            model.cell = Session["cell"].ToString();
            model.email = Session["email"].ToString();
            model.fullname = model.firstname.Replace(" ", "") + " " + model.lastname.Replace(" ", "");
            int id = inserttempBusinesslead(model, Request.UserHostAddress);
            Session["id"] = id;
                Session["amazon"] = true;
            Session["type"] = "d";
            if (Session["formlead"] != null)
            {
                int uid = Convert.ToInt32(Session["formlead"].ToString());
                updateleadpaymentpage(uid);
            }
            return View(model);
            }
            catch (Exception ex)
            {

            }
            return View();
        }




        public ActionResult m_checkout()
        {
            Session["url"] = "https://travelrewardsvacations.com/special/fbamazon/m_checkout";
            if (Session["firstname"] == null)
            {
                return RedirectToAction("index", "main");
            }
            formmodal model = new Models.formmodal();
            model.firstname = Session["firstname"].ToString();
            model.lastname = Session["lastname"].ToString();
            model.cell = Session["cell"].ToString();
            model.email = Session["email"].ToString();
            model.fullname = model.firstname.Replace(" ", "") + " " + model.lastname.Replace(" ", "");
            int id = inserttempBusinesslead(model, Request.UserHostAddress);
            Session["id"] = id;
            Session["type"] = "m";
            Session["amazon"] = true;
            if (Session["formlead"] != null)
            {
                int uid = Convert.ToInt32(Session["formlead"].ToString());
                updateleadpaymentpage(uid);
            }
            return View(model);
        }

        public void updateleadpaymentpage(int id)
        {
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@id", id);
                _dt.executeNonQuery("updateleadpaymentpage", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }
        public void updatelead(int userid, int expirymonth, int expiryday, string cvv, string cardnumber, string cardtype, formmodal model, int planid)
        {
            SqlHelper _dt = new SqlHelper();
            var crypr = new Crypto();
            try
            {
                SortedList _srtlist = new SortedList();
                string username = model.email;
                string password = !string.IsNullOrEmpty(model.cell) ? crypr.EncryptStringAES(model.cell) : "";
                _srtlist.Add("@userid", userid);
                _srtlist.Add("@cvv", crypr.EncryptStringAES("0000"));
                _srtlist.Add("@Cardnumber", crypr.EncryptStringAES("xxxx-xxxx-xxxx-xxxx"));
                _srtlist.Add("@Expirymonth", 0);
                _srtlist.Add("@ExpiryYear", 0);
                _srtlist.Add("@CardType", crypr.EncryptStringAES("xxxx"));
                _srtlist.Add("@city", "");
                _srtlist.Add("@state", "");
                _srtlist.Add("@zip", "");
                _srtlist.Add("@address", "");
                _srtlist.Add("@planid", planid);
                _srtlist.Add("@username", username);
                _srtlist.Add("@password", password);
                _srtlist.Add("@amazonorder", model.amazonorder);
                _dt.executeNonQuery("updateuserwithcardDetails", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }

        public bool validateUser(int userid, ref int user, string amzonordernumber = "")
        {
            SqlHelper _sql = new SqlHelper();
            SortedList _srtlist = new SortedList();
            formmodal _model = new formmodal();
            _srtlist.Add("@UserId", userid);
            var dt = _sql.fillDataTable("Adduserfromtemp", "", _srtlist);
            if (dt.Rows.Count > 0)
            {
                Session["price"] = "9.99";
                Session["lastname"] = dt.Rows[0]["LName"].ToString();
                Session["email"] = dt.Rows[0]["Email"].ToString();
                Session["cell"] = dt.Rows[0]["Mobile"].ToString();
                Session["firstname"] = dt.Rows[0]["fName"].ToString();
                _srtlist.Clear();
                _srtlist.Add("@Address", "");
                _srtlist.Add("@City", "");
                _srtlist.Add("@email", dt.Rows[0]["Email"].ToString());
                _srtlist.Add("@Fname", dt.Rows[0]["fName"].ToString().Replace(" ", ""));
                _srtlist.Add("@Lname", dt.Rows[0]["LName"].ToString().Replace(" ", ""));
                _srtlist.Add("@MobileNumber", dt.Rows[0]["Mobile"].ToString());
                _srtlist.Add("@State", "");
                _srtlist.Add("@ZipCode", "");
                _srtlist.Add("@isbusinessprofile", true);
                _srtlist.Add("@ip", dt.Rows[0]["host"].ToString());
                _srtlist.Add("@planid", 0);
                _srtlist.Add("@businessemail", dt.Rows[0]["Email"].ToString());
                _srtlist.Add("@businessname", dt.Rows[0]["fName"].ToString().Replace(" ", "") + " " + dt.Rows[0]["LName"].ToString().Replace(" ", ""));
                _srtlist.Add("@question1", "0");
                _srtlist.Add("@question2", "N");
                _srtlist.Add("@question3", "N");
                _srtlist.Add("@question4", "N");
                var id = Convert.ToInt32(_sql.executeNonQueryWMessage("AddbsinessLead", "", _srtlist));
                Session["id"] = id;
                user = id;
                _model.email = dt.Rows[0]["Email"].ToString();
                _model.cell = dt.Rows[0]["Mobile"].ToString();
                _model.amazonorder = amzonordernumber;
                Task.Factory.StartNew(() => updatelead(id, 0, 0, "", "", "", _model, 6));
                return true;
            }
            else
            {
                return false;
            }
        }

        public int insertBusinesslead(formmodal model, string host, int planid, bool IsBusinessProfile = true)
        {
            int id = 0;
            SqlHelper _dt = new SqlHelper();
            try
            {

                SortedList _srtlist = new SortedList();
                _srtlist.Add("@Address", "");
                _srtlist.Add("@City", "");
                _srtlist.Add("@email", model.email);
                _srtlist.Add("@Fname", model.firstname.Replace(" ", ""));
                _srtlist.Add("@Lname", model.lastname.Replace(" ", ""));
                _srtlist.Add("@MobileNumber", model.cell);
                _srtlist.Add("@State", "");
                _srtlist.Add("@ZipCode", "");
                _srtlist.Add("@isbusinessprofile", IsBusinessProfile);
                _srtlist.Add("@ip", host);
                _srtlist.Add("@planid", 0);
                _srtlist.Add("@businessemail", model.email);
                _srtlist.Add("@businessname", model.firstname.Replace(" ", "") + " " + model.lastname.Replace(" ", ""));
                _srtlist.Add("@question1", "0");
                _srtlist.Add("@question2", "N");
                _srtlist.Add("@question3", "N");
                _srtlist.Add("@question4", "N");
                id = Convert.ToInt32(_dt.executeNonQueryWMessage("AddbsinessLead", "", _srtlist));
                return id;
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                return 0;
            }
            finally
            {
                _dt.Dispose();
            }
        }
        public bool validateamazonnumber(string number, ref bool isused)
        {
            isused = false;
            SqlHelper _sql = new SqlHelper();
            var crypr = new Crypto();
            bool _stringlist = false;
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@amazonordernumber", number);
                var _dt = _sql.fillDataTable("getvalidatenumbers", "", _srt);
                if (_dt.Rows.Count > 0)
                {
                    _stringlist = true;
                    isused = _dt.Rows[0][0].ToString().ToLower() == "y" ? true : false;
                }
                _dt.Dispose();
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {

            }
            return _stringlist;
        }
        public void insertpaymentlog(int userId, double amount, int planid, bool issetupfee, int billingdays, string status, string paymentlog)
        {
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@userId", userId);
                _srtlist.Add("@Planid", planid);
                _srtlist.Add("@issetupfee", issetupfee);
                _srtlist.Add("@amount", amount);
                _srtlist.Add("@billingdays", billingdays);
                _srtlist.Add("@status", status);
                _srtlist.Add("@paymentlog", amount.ToString() + "$ " + paymentlog);
                _dt.executeNonQuery("addbilling", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }

        public int inserttempBusinesslead(formmodal model, string host)
        {
            int id = 0;
            SqlHelper _dt = new SqlHelper();
            try
            {

                SortedList _srtlist = new SortedList();
                _srtlist.Add("@email", model.email);
                _srtlist.Add("@Fname", model.firstname.Replace(" ", ""));
                _srtlist.Add("@Lname", model.lastname.Replace(" ", ""));
                _srtlist.Add("@MobileNumber", model.cell);
                _srtlist.Add("@ip", host);
                id = Convert.ToInt32(_dt.executeNonQueryWMessage("AddbsinesstempLead", "", _srtlist));
                return id;
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                return 0;
            }
            finally
            {
                _dt.Dispose();
            }
        }
        public ActionResult approved(string orderReferenceId = "", string sellerOrderId = "")
        {
            Session["number"] = orderReferenceId;
            Session["price"] = "9.99";
            int userid = 0;
            bool validate = validateUser(Convert.ToInt32(sellerOrderId), ref userid, orderReferenceId);
            if (validate)
            {
                bool isused = false;
                var isavailable = validateamazonnumber(orderReferenceId, ref isused);
                if (isused)
                {
                    TempData["error"] = @"Your Amazon Order Number has already been used to setup an account.  Please check your email for your Unlimited Access Vacation Code,  or contact Customer Support for additional information at support@claimyourvacations.com.  ";
                    insertpaymentlog(userid, Convert.ToDouble("9.99"), 6, false, 30, "Decline", "Decline - Amazon Order Number Used ");
                    return RedirectToAction("index", "declinse");
                }
                else
                {
                    string log = "Payment done Succesfully--  Status--- Sucess";
                    insertpaymentlog(userid, Convert.ToDouble("9.99"), 6, false, 30, "approved", log);
                    return RedirectToAction("index", "thanks_you");

                }
            }
            else
            {
                TempData["error"] = @"Your Unlimited Access account has already been created.  Please check your email for your Unlimited Access Vacation Code,  or contact Customer Support for additional information at support@claimyourvacations.com.  ";
                insertpaymentlog(userid, Convert.ToDouble("9.99"), 6, false, 30, "Decline", "User Id Not FOund");
                return RedirectToAction("index", "declinse");
            }

        }


        public ActionResult cancel(string seller, string failureCode, string type)
        {
            string sellerOrderId = seller;
            int userid = 0;
            bool validate = validateUser(Convert.ToInt32(sellerOrderId), ref userid, "");
            if (validate)
            {
                string url = type == "m" ? "https://travelrewardsvacations.com/special/fbamazon/m_checkout" : "https://travelrewardsvacations.com/special/fbamazon/checkout";
                TempData["error"] = @"Unfortunately, the payment $9.99 could not be processed.  Please <a href='" + url + "'> CLICK HERE </a> to confirm your Amazon Pay information was entered correctly.  ";
                insertpaymentlog(userid, Convert.ToDouble("9.99"), 6, false, 30, "Declined", failureCode);
                return RedirectToAction("index", "declinse");
            }
            else
            {
                TempData["error"] = @"Your Unlimited Access account has already been created.  Please check your email for your Unlimited Access Vacation Code,  or contact Customer Support for additional information at support@claimyourvacations.com.  ";
                insertpaymentlog(userid, Convert.ToDouble("9.99"), 6, false, 30, "Decline", "User Id Not FOund");
                return RedirectToAction("index", "declinse");
            }
        }

        ///----Amazon helper--------------------------

        public JsonResult AddRequiredParameters(string CurrencyCode, string SellerNote, string Amount = "9.99")
        {
            // Mandatory fields
            string sellerId = "AIY3BL04F3TM0";
            string accessKey = "AKIAJQEOFOAAKODMIJ5Q";
            string secretKey = "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";
            string lwaClientId = "amzn1.application-oa2-client.2de7e1bc4bac4f68be476fd2137e2425";

            if (String.IsNullOrEmpty(sellerId))
                throw new ArgumentNullException("sellerId", "sellerId is NULL, set the value in the configuration file ");
            if (String.IsNullOrEmpty(accessKey))
                throw new ArgumentNullException("accessKey", "accessKey is NULL, set the value in the configuration file ");
            if (String.IsNullOrEmpty(secretKey))
                throw new ArgumentNullException("secretKey", "secretKey is NULL, set the value in the configuration file ");
            if (String.IsNullOrEmpty(lwaClientId))
                throw new ArgumentNullException("lwaClientId", "lwaClientId is NULL, set the value in the configuration file ");

            string amount = Amount;
            string currencyCode = CurrencyCode;
            string sellerNote = SellerNote;
            string sellerOrderId = Session["id"].ToString();
            string shippingAddressRequired = "false";
            string paymentAction = "AuthorizeAndCapture";
            string type = Session["type"].ToString();
            string returnURL = "https://travelrewardsvacations.com/special/fbamazon/approved";
            string cancelReturnURL = "https://travelrewardsvacations.com/special/fbamazon/cancel?seller=" + sellerOrderId + "&type=" + type;

            // Optional fields


            IDictionary<String, String> parameters = new Dictionary<String, String>();
            parameters.Add("accessKey", accessKey);
            parameters.Add("sellerId", sellerId);
            parameters.Add("amount", amount);
            parameters.Add("returnURL", returnURL);
            parameters.Add("cancelReturnURL", cancelReturnURL);
            parameters.Add("lwaClientId", lwaClientId);
            parameters.Add("sellerNote", sellerNote);
            parameters.Add("sellerOrderId", sellerOrderId);
            parameters.Add("currencyCode", currencyCode);
            parameters.Add("shippingAddressRequired", shippingAddressRequired);
            parameters.Add("paymentAction", paymentAction);

            string Signature = SignParameters(parameters, secretKey);

            IDictionary<String, String> SortedParameters =
                      new SortedDictionary<String, String>(parameters, StringComparer.Ordinal);
            SortedParameters.Add("signature", UrlEncode(Signature, false));

            //var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //return (jsonSerializer.Serialize());
            return Json(SortedParameters, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getListOrderAPIRequiredParameters()
        {
            // Mandatory fields
            string sellerId = "AIY3BL04F3TM0";
            string accessKey = "AKIAJQEOFOAAKODMIJ5Q";
            string secretKey = "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";
            // string lwaClientId = "amzn1.application-oa2-client.2de7e1bc4bac4f68be476fd2137e2425";
            IDictionary<String, String> parameters = new Dictionary<String, String>();
            parameters.Add("AWSAccessKeyId", accessKey);
            parameters.Add("Action", "ListOrders");
            parameters.Add("MWSAuthToken", "amzn.mws.92b6c8fa-073b-0c09-8d82-04fe2819efe0");
            parameters.Add("sellerId", sellerId);
            parameters.Add("MarketplaceId.Id.1", "A3BXB0YN3XH17H");
            parameters.Add("CreatedAfter ", DateTime.UtcNow.AddHours(-5).ToString("yyyy-MM-dd'T'HH:mm:ss.fffK"));
            parameters.Add("Timestamp", DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ").Replace(".", ":"));
            parameters.Add("Version", "2013-09-01");
            parameters.Add("SignatureVersion", "2");
            parameters.Add("SignatureMethod", "HmacSHA256");
            string Signature = SignParameters(parameters, secretKey);
            //IDictionary<String, String> SortedParameters =
            //          new SortedDictionary<String, String>(parameters, StringComparer.Ordinal);
            //SortedParameters.Add("signature", UrlEncode(Signature, false));

            var url = @"https://mws.amazonservices.jp/Orders/2013-09-01?AWSAccessKeyId=" + accessKey + "&Action=ListOrders&MWSAuthToken=amzn.mws.92b6c8fa-073b-0c09-8d82-04fe2819efe0&MarketplaceId.Id.1 =A3BXB0YN3XH17H&SellerId=" + sellerId + "&Signature=" + UrlEncode(Signature, false) + "&SignatureVersion=2&SignatureMethod=HmacSHA256&CreatedAfter=" + DateTime.UtcNow.AddHours(-5).ToString("yyyy-MM-dd'T'HH:mm:ss.fffK") + "&Timestamp=" + DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ").Replace(".", ":") + "&Version=2013-09-01";
            return Redirect(url);
            //var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //return (jsonSerializer.Serialize());
            // return Json(SortedParameters, JsonRequestBehavior.AllowGet);
        }


        /**
          * Convert Dictionary of parameters to URL encoded query string
          */
        private string GetParametersAsString(IDictionary<String, String> parameters)
        {
            StringBuilder data = new StringBuilder();
            foreach (String key in (IEnumerable<String>)parameters.Keys)
            {
                String value = parameters[key];
                if (value != null)
                {
                    data.Append(key);
                    data.Append('=');
                    data.Append(UrlEncode(value, false));
                    data.Append('&');
                }
            }
            String result = data.ToString();
            return result.Remove(result.Length - 1);
        }

        private String SignParameters(IDictionary<String, String> parameters, String key)
        {
            String signatureVersion = "2";
            KeyedHashAlgorithm algorithm = new HMACSHA256();
            String stringToSign = null;
            if ("2".Equals(signatureVersion))
            {
                String signatureMethod = "HmacSHA256";
                algorithm = KeyedHashAlgorithm.Create(signatureMethod.ToUpper());
                stringToSign = CalculateStringToSignV2(parameters);
            }
            else
            {
                throw new Exception("Invalid Signature Version specified");
            }
            return Sign(stringToSign, key, algorithm);
        }



        private String CalculateStringToSignV2(IDictionary<String, String> parameters)
        {
            StringBuilder data = new StringBuilder();
            IDictionary<String, String> sorted =
                  new SortedDictionary<String, String>(parameters, StringComparer.Ordinal);
            data.Append("POST");
            data.Append("\n");
            data.Append("payments.amazon.com");
            data.Append("\n");
            data.Append("/");
            data.Append("\n");
            foreach (KeyValuePair<String, String> pair in sorted)
            {
                if (pair.Value != null)
                {
                    data.Append(UrlEncode(pair.Key, false));
                    data.Append("=");
                    data.Append(UrlEncode(pair.Value, false));
                    data.Append("&");
                }

            }

            String result = data.ToString();
            return result.Remove(result.Length - 1);
        }


        private String UrlEncode(String data, bool path)
        {
            StringBuilder encoded = new StringBuilder();
            String unreservedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~" + (path ? "/" : "");

            foreach (char symbol in System.Text.Encoding.UTF8.GetBytes(data))
            {
                if (unreservedChars.IndexOf(symbol) != -1)
                {
                    encoded.Append(symbol);
                }
                else
                {
                    encoded.Append("%" + String.Format("{0:X2}", (int)symbol));
                }
            }

            return encoded.ToString();

        }

        /**
         * Computes RFC 2104-compliant HMAC signature.
         */
        private String Sign(String data, String key, KeyedHashAlgorithm algorithm)
        {
            Encoding encoding = new UTF8Encoding();
            algorithm.Key = encoding.GetBytes(key);
            return Convert.ToBase64String(algorithm.ComputeHash(
                encoding.GetBytes(data.ToCharArray())));
        }

    }
}