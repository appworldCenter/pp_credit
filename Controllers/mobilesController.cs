﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using static WebApplication1.Controllers.mainController;

namespace WebApplication1.Controllers
{
    public class mobilesController : Controller
    {
        public ActionResult step1()
        {
            formmodal _model = new formmodal();
            if (Session["firstname"] != null)
            {
                _model.firstname = FirstCharToUpper(Session["firstname"].ToString());
                Session["firstname"] = null;
            }
            if (Session["lastname"] != null)
            {
                _model.lastname = FirstCharToUpper(Session["lastname"].ToString());
                Session["lastname"] = null;
            }
            if (Session["email"] != null)
            {
                _model.email = Session["email"].ToString();
                Session["email"] = null;
            }

            if (Session["zipcode"] != null)
            {
                _model.zip = Session["zipcode"].ToString();
                Session["zipcode"] = null;
            }
            if (Session["dob"] != null)
            {
                _model.dob = Session["dob"].ToString();
                Session["dob"] = null;
            }
            if (Session["gender"] != null)
            {
                _model.gender = Session["gender"].ToString();
                Session["gender"] = null;
            }
            if (Session["state"] != null)
            {
                _model.state = Session["state"].ToString();
                Session["state"] = null;
            }
            if (Session["city"] != null)
            {
                _model.city = Session["city"].ToString();
                Session["city"] = null;
            }
            if (Session["address"] != null)
            {
                _model.address = Session["address"].ToString();
                Session["address"] = null;
            }
            if (Session["phone"] != null)
            {
                _model.cell = Session["phone"].ToString();
                Session["cell"] = Session["phone"].ToString();
            }
            return View(_model);
        }
        public string FirstCharToUpper(string input)
        {
            return string.IsNullOrEmpty(input)?"": input.First().ToString().ToUpper() + input.Substring(1);
        }
        public Root IsBadIp(ref string _response, string ip = "", string user_agent = "")
        {
            ip = string.IsNullOrEmpty(ip) ? Request.UserHostAddress : ip;
            user_agent = string.IsNullOrEmpty(user_agent) ? Request.UserAgent : user_agent;
            var url = $"https://ipqualityscore.com/api/json/ip/L8A7fsYYUIrM8iPW19Xq9iIKGhMoXKMe/{ip}?strictness=0&allow_public_access_points=true&user_agent={user_agent}&user_language=en-US&campaignID=VSGFT";
            _response = Get(url);
            var data = JsonConvert.DeserializeObject<Root>(_response);
            _response += " Url: " + url;
            return data;
        }
        [HttpPost]
        public string step1offer(formmodal _model)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
             | SecurityProtocolType.Tls11
             | SecurityProtocolType.Tls12
             | SecurityProtocolType.Ssl3;
            string biltylink = "";
            bool isLinkValid = true;
            Session["firstname"] = FirstCharToUpper(_model.firstname);
            Session["lastname"] = FirstCharToUpper(_model.lastname);
            Session["email"] = _model.email;
            Session["zipcode"] = _model.zip;
            Session["cell"] = _model.cell;
            string utm_source = Session["utm_source"] == null ? "" : Session["utm_source"].ToString();
            try
            {
                string _res = "";
                var _data = IsBadIp(ref _res, Request.UserHostAddress, Request.UserAgent); 
                if (_data.fraud_score > 50) 
                {
                    string responseString = "";
                    if (!string.IsNullOrEmpty(_model.cell))
                    {
                        var response = "https://getultimateoffer.com/V1/ismobile?Number=" + _model.cell.Replace('-', ' ').Replace(" ", "").Substring(0, 6);
                        if (_model.autoclick > 0)
                        {
                            responseString = "{'ResponseCode':' - 14','ResponseMeaning':'Duplicate Alert, Lead Is Exist in DB','ResponseText':'Duplicate Leads'}";
                        }
                        Session["response"] = responseString;
                        string id = new Helper().inserts2swithunid(_model, Request.UserHostAddress, responseString, false, utm_source, "", "", "SSO", _model.cell, Quality: (!isLinkValid ? "Link Data" : "Blocked IP"));
                       
                    } 
                    biltylink = "disabled"; //return View("", _model);
                }
                else
                {
                    biltylink = "https://spnccrzone.com/?eqi=0ugnf0Mj%2fEvAniErwl1pkKfOsoiKmWYblgDJwpjxrOw%3d&s1=SSO&s2=";
                    if (!string.IsNullOrEmpty(_model.cell))
                       { 
                        string fbid = Session["fbid"] == null ? "" : Session["fbid"].ToString();
                        if (Session["formlead"] != null)
                        {
                            int leadid = Convert.ToInt32(Session["formlead"]);
                            updateformlead(leadid);
                            //updateleadpaymentpage(leadid);
                        }
                        else
                        { 
                            if (!string.IsNullOrEmpty(_model.cell) && !string.IsNullOrEmpty(_model.email))
                            {
                                _model.firstname = FirstCharToUpper(_model.firstname);
                                isLinkValid = !string.IsNullOrEmpty(_model.firstname) ? isLinkValid : false;
                                _model.lastname = FirstCharToUpper(_model.lastname);
                                isLinkValid = (!string.IsNullOrEmpty(_model.lastname) || !_model.lastname.Contains("lastname")) ? isLinkValid : false;
                                _model.cell = _model.cell.Replace('-', ' ').Replace(" ", "");
                                isLinkValid = (!string.IsNullOrEmpty(_model.cell) || !_model.cell.Contains("number")) ? isLinkValid : false;
                                Session["fullname"] = _model.firstname.Replace(" ", "") + " " + _model.lastname.Replace(" ", "");
                                Session["lastname"] = _model.lastname.Replace(" ", "");
                                Session["firstname"] = _model.firstname.Replace(" ", "");
                                Session["cell"] = _model.cell;
                                Session["email"] = _model.email;
                                Session["zip"] = Session["zipcode"] == null ? "" : Session["zipcode"].ToString(); ;
                                isLinkValid = (!string.IsNullOrEmpty(_model.email) || !_model.email.Contains("email")) ? isLinkValid : false;
                                _model.zip = Session["zip"].ToString();
                                _model.dob = Session["dob"] == null ? "" : Session["dob"].ToString();// "07/18/2019"
                                _model.gender = Session["gender"] == null ? "" : Session["gender"].ToString();
                                _model.state = Session["state"] == null ? "" : Session["state"].ToString();
                                _model.city = Session["city"] == null ? "" : Session["city"].ToString();
                                _model.address = Session["address"] == null ? "" : Session["address"].ToString();
                                isLinkValid = !string.IsNullOrEmpty(_model.dob) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.gender) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.state) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.city) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.address) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.zip) ? isLinkValid : false;
                                var response = "https://getultimateoffer.com/V1/ismobile?Number=" + _model.cell.Replace('-', ' ').Replace(" ", "").Substring(0, 6);
                                string responseString = Get(response);
                                string unid = "";
                                string clickid = "";
                                if (Session["unid"] != null)
                                {
                                    unid = Convert.ToString(Session["unid"]);
                                    unid = unid.Replace('-', ' ').Replace(" ", "");
                                    if (unid.Contains("number") || string.IsNullOrEmpty(unid))
                                        isLinkValid = false;
                                }
                                else
                                {
                                    isLinkValid = false;
                                }
                                if (Session["utm_source"] != null)
                                {
                                    utm_source = Convert.ToString(Session["utm_source"]);
                                    if (string.IsNullOrEmpty(utm_source))
                                        isLinkValid = false;
                                }
                                else
                                {
                                    isLinkValid = false;
                                }
                                if (Session["clickid"] != null)
                                {
                                    clickid = Convert.ToString(Session["clickid"]);
                                    if (string.IsNullOrEmpty(clickid))
                                        isLinkValid = false;
                                }
                                else
                                {
                                    isLinkValid = false;
                                }
                                if (isunidExist(unid) > 0)
                                {
                                    responseString = "{'ResponseCode':' - 14','ResponseMeaning':'Duplicate Alert, Lead Is Exist in DB','ResponseText':'Duplicate Leads'}";
                                }
                                Session["response"] = responseString;
                                string id = new Helper().inserts2swithunid(_model, Request.UserHostAddress, responseString, false, utm_source, clickid, "", fbid, unid, Quality: isLinkValid ? "Link Data" : "Valid");
                                Session["formlead"] = id;
                                updateformlead(Convert.ToInt32(id));
                                //updateleadpaymentpage(leadid);
                            }
                        }
                        var newgender = "M";
                       
                        if (!string.IsNullOrEmpty(_model.cell))
                        {
                            biltylink = biltylink + "&s3=" + _model.cell;
                            biltylink = biltylink + "&phonecode=" + _model.cell.Substring(0, 3) + "&phoneprefix=" + _model.cell.Substring(3, 3) + "&phonesuffix=" + _model.cell.Substring(6);
                            biltylink = biltylink + "&address2=United+States&telephone=" + _model.cell;
                        }
                        if (!string.IsNullOrEmpty(_model.firstname))
                        {
                            biltylink = biltylink + "&firstname=" + _model.firstname.Replace(" ", "");
                        }
                        if (!string.IsNullOrEmpty(_model.lastname))
                        {
                            biltylink = biltylink + "&lastname=" + _model.lastname.Replace(" ", "");
                        }
                        if (!string.IsNullOrEmpty(_model.email))
                        {
                            biltylink = biltylink + "&email=" + _model.email;
                        }
                        if (!string.IsNullOrEmpty(_model.zip))
                        {
                            biltylink = biltylink + "&zippost=" + _model.zip;
                        }
                        if (!string.IsNullOrEmpty(_model.address))
                        {
                            biltylink = biltylink + "&address1=" + _model.address;
                        }
                        if (!string.IsNullOrEmpty(_model.city))
                        {
                            biltylink = biltylink + "&city=" + _model.city;
                        }
                        if (!string.IsNullOrEmpty(_model.state))
                        {
                            biltylink = biltylink + "&state=" + _model.state;
                        }
                        if (!string.IsNullOrEmpty(_model.gender))
                        {
                            newgender = (_model.gender.ToLower() == "m" || _model.gender.ToLower() == "male") ? "M" : "F";
                            biltylink = biltylink + "&gender=" + newgender;
                        }
                        if (Session["dobmonth"] != null)
                        {
                            biltylink = biltylink + "&dobmonth=" + Session["dobmonth"].ToString();
                            biltylink = biltylink + "&dobday=" + Session["dobday"].ToString();
                            biltylink = biltylink + "&dobyear=" + Session["dobyear"].ToString();
                        }
                        insertpaymentlog(1, Convert.ToDouble("1"), 6, true, 30, "Link", biltylink, _model.email, _model.firstname.Replace(" ", "") + " " + _model.lastname.Replace(" ", ""), _model.cell);
                        ViewBag.click = biltylink;
                    }
                }
                _res += " biltylink:" + biltylink;
                 if (System.IO.File.Exists( Server.MapPath("~/App_Data/autoresponderupdateBody1.xml")))
                {
                    System.IO.File.Delete( Server.MapPath("~/App_Data/autoresponderupdateBody1.xml"));
                }
                System.IO.File.WriteAllText( Server.MapPath("~/App_Data/autoresponderupdateBody1.xml"), _res);

                return biltylink;
            }
            catch (Exception ex)
            {
                return ex.Message + " " + ex.StackTrace;
            }

        }
        public int isunidExist(string unid = "")
        {
            int isexist = 0;
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@unid", unid);
                var _count = _dt.executeNonQueryWMessage("checkunid", "", _srtlist).ToString();
                isexist = Convert.ToInt16(_count);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
            return isexist;
        }

        public string inserts2sformlead(formmodal model, string host, string response = "", bool iss2spost = false, string utm_source = "", string clickid = "", string leadresponse = "", string subid = "")
        {
            string mes = "";
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@email", model.email);
                _srtlist.Add("@dob", model.dob);
                _srtlist.Add("@Fname", FirstCharToUpper(model.firstname.Replace(" ", "")));
                _srtlist.Add("@Lname", FirstCharToUpper(model.lastname.Replace(" ", "")));
                _srtlist.Add("@MobileNumber", model.cell);
                _srtlist.Add("@leadresponse", response);
                _srtlist.Add("@ispost", false);
                _srtlist.Add("@ip", host);
                _srtlist.Add("@utm_source", utm_source);
                _srtlist.Add("@clickid", clickid);
                _srtlist.Add("@fbid", model.fbid);
                mes = _dt.executeNonQueryWMessage("Addnews2slog4", "", _srtlist).ToString();
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
            return mes;
        }

        public void updateleadpaymentpage(int id)
        {
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@id", id);
                _dt.executeNonQuery("updateleadpaymentpage", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }
        public void updateformlead(int id)
        {
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@id", id);
                _dt.executeNonQuery("updatelogs", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }

        public ActionResult promo(int dobmonth = 0, int dobday = 0, int dobyear = 0, string gender = "", string fbid = "", string subid = "", string firstname = "", string lastname = "", string phone = "", string email = "", string zipcode = "", string street = "", string city = "", string state = "", int c = 0)
        {
            var newgender = "M";
            string biltylink = "https://spnccrzone.com/?eqi=0ugnf0Mj%2fEvAniErwl1pkKfOsoiKmWYblgDJwpjxrOw%3d&s1=flnt2&s2=" + fbid;
            if (!string.IsNullOrEmpty(phone))
            {
                biltylink = biltylink + "&s3=" + phone;
                biltylink = biltylink + "&phonecode=" + phone.Substring(0, 3) + "&phoneprefix=" + phone.Substring(3, 3) + "&phonesuffix=" + phone.Substring(6);
                biltylink = biltylink + "&address2=United+States&telephone=" + phone;
            }
            if (!string.IsNullOrEmpty(firstname))
            {
                biltylink = biltylink + "&firstname=" + firstname.Replace(" ", "");
            }
            if (!string.IsNullOrEmpty(lastname))
            {
                biltylink = biltylink + "&lastname=" + lastname.Replace(" ", "");
            }
            if (!string.IsNullOrEmpty(email))
            {
                biltylink = biltylink + "&email=" + email;
            }
            if (!string.IsNullOrEmpty(zipcode))
            {
                biltylink = biltylink + "&zippost=" + zipcode;
            }
            if (!string.IsNullOrEmpty(street))
            {
                biltylink = biltylink + "&address1=" + street.Replace(" ", "%20").Replace("%20", "+");
            }
            if (!string.IsNullOrEmpty(city))
            {
                biltylink = biltylink + "&city=" + city;
            }
            if (!string.IsNullOrEmpty(state))
            {
                biltylink = biltylink + "&state=" + state;
            }
            if (!string.IsNullOrEmpty(gender))
            {
                newgender = (gender.ToLower() == "m" || gender.ToLower() == "male") ? "M" : "F";
                biltylink = biltylink + "&gender=" + newgender;
            }
            if (dobmonth != 0)
            {
                biltylink = biltylink + "&dobmonth=" + dobmonth;
                biltylink = biltylink + "&dobday=" + dobday;
                biltylink = biltylink + "&dobyear=" + dobyear;
            }
            string alerturl = "https://getultimateoffer.com/user/getalertmessage?fname=" + firstname + "&lname=" + lastname + "&phone=" + phone + "&email=" + email + "&c=" + c;
            trigger_success(phone, email, street, city, firstname, lastname, state, zipcode);
            insertpaymentlog(1, Convert.ToDouble("1"), 6, true, 30, "Link", biltylink);
            Get(alerturl);
            //  Task.Factory.StartNew(() => ).ContinueWith((alert) => ).ContinueWith((bitlyLink) => );
            return Redirect(biltylink);
        }
        public void insertpaymentlog(int userId, double amount, int planid, bool issetupfee, int billingdays, string status, string paymentlog, string email = "", string name = "", string mobile = "")
        {
            SqlHelper _dt = new SqlHelper();
            var clickid = "";
            var subid = Session["fbid"] == null ? "flnt2" : Convert.ToString(Session["fbid"]);
            var ip = Request.UserHostAddress;
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@mobile", mobile);
                _srtlist.Add("@name", name);
                _srtlist.Add("@email", email);
                _srtlist.Add("@clickid", clickid);
                _srtlist.Add("@subid", subid);
                _srtlist.Add("@ip", ip);
                _srtlist.Add("@userId", userId);
                _srtlist.Add("@Planid", planid);
                _srtlist.Add("@issetupfee", issetupfee);
                _srtlist.Add("@amount", amount);
                _srtlist.Add("@billingdays", billingdays);
                _srtlist.Add("@status", status);
                _srtlist.Add("@paymentlog", paymentlog);
                _dt.executeNonQuery("addbilling", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }
        public string Standard_Busines_Plan_v2(List<string> list)
        {
            string body = "<div class='col-md-12' align='left'><h4 style = 'text-align: justify;'><span style='color: #45818E;'>";
            body = body + "Congratulations! Your Unlimited Lifetime Access to our Complimentary Vacations is now Active.  </h4></span>";

            body = body + "<p>The following Vacation Code has unlimited access to all of our vacations and it can be shared with family and friends.  It's your choice! </p>";
            body = body + "<h4 style = 'color:black !important' >";
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    body = body + "<br/> " + item;
                }
            }
            body = body + "</h4>";
            body = body + "<p><a href='https://ActivateYourVacations.com'>www.ActivateYourVacations.com</a></p>";
            body = body + @"This Vacation Code does not expire and has unlimited use. Once the user has selected their vacations on <a href='https://www.ActivateYourVacations.com'>www.ActivateYourVacations.com</a>, the user will be emailed their online reservation form. The end user will then have one year to book their vacation. Please check your spam folder if you do not receive our confirmation email in your inbox, and save this information for future reference. We also recommend adding our Customer Support email <a href='mailto:support@travelvacationservices.com'>support@travelvacationservices.com</a> to your email contact list to avoid our emails going into your spam folders. Thank You. </p>";
            body = body + "<p>Thank you and enjoy your Vacations</p>";
            body = body + "<h4 style = 'text-align: justify;margin:0px !important;' ><span style= 'color: #45818E;' ><strong> Client Services</strong></span>";
            body = body + "</h4><h4 style = 'text-align: justify;margin:0px !important;' ><span style= 'color: #45818E;' ><strong>Travel Vacation Services</strong></span></h4></div>";
            return body;

        }
        public int insertBusinesslead(formmodal model, string host, int planid, bool IsBusinessProfile = true)
        {
            int id = 0;
            SqlHelper _dt = new SqlHelper();
            var clickid = "";
            var subid = "";
            //try
            //{
            SortedList _srtlist = new SortedList();
            _srtlist.Add("@Address", model.address);
            _srtlist.Add("@City", model.city);
            _srtlist.Add("@clickid", clickid);
            _srtlist.Add("@subid", subid);
            _srtlist.Add("@email", model.email);
            _srtlist.Add("@Fname", FirstCharToUpper(model.firstname));
            _srtlist.Add("@Lname", FirstCharToUpper(model.lastname));
            _srtlist.Add("@MobileNumber", model.cell);
            _srtlist.Add("@State", model.state);
            _srtlist.Add("@ZipCode", model.zip);
            _srtlist.Add("@isbusinessprofile", IsBusinessProfile);
            _srtlist.Add("@ip", host);
            _srtlist.Add("@planid", 0);
            _srtlist.Add("@businessemail", model.email);
            _srtlist.Add("@businessname", model.firstname + " " + model.lastname);
            _srtlist.Add("@question1", "0");
            _srtlist.Add("@question2", "N");
            _srtlist.Add("@question3", "N");
            _srtlist.Add("@question4", "N");
            id = Convert.ToInt32(_dt.executeNonQueryWMessage("AddspecialbsinessLead", "", _srtlist));
            return id;
            //}
            //catch (Exception ex)
            //{
            //    var a = ex.Message;
            //    return 0;
            //}
            //finally
            //{
            //    _dt.Dispose();
            //}
        }
        public string trigger_success(string mobile, string email, string address, string city, string fname, string lname, string state, string zip)
        {
            try
            {
                formmodal model = new formmodal();
                string host = Request.UserHostAddress;
                model.cell = mobile;
                model.email = email;
                model.address = address;
                model.city = city;
                model.firstname = FirstCharToUpper(fname);
                model.lastname = FirstCharToUpper(lname);
                model.state = state;
                model.zip = zip;
                int id = insertBusinesslead(model, host, 6);
                if (id != 0)
                {
                    insertpaymentlog_v2(id, Convert.ToDouble("19.9"), 6, true, 30, "approved", "test");

                    var usedlist = updateBusinesslead(id);
                    string body = Standard_Busines_Plan_v2(usedlist);
                    updatestandardlead(id);
                    //insertemaillog(fname, lname, email, mobile, body);
                    //Sendmail(email, body, "Unlimited Access to Vacations and Cruises", "Travel Services");
                    addemail(fname, lname, email, mobile, body, "Unlimited Access to Vacations and Cruises", "Travel Services");
                }
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public void insertpaymentlog_v2(int userId, double amount, int planid, bool issetupfee, int billingdays, string status, string paymentlog)
        {
            SqlHelper _dt = new SqlHelper();
            var clickid = "";
            var subid = "";
            var ip = Request.UserHostAddress;
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@clickid", clickid);
                _srtlist.Add("@subid", subid);
                _srtlist.Add("@ip", ip);
                _srtlist.Add("@userId", userId);
                _srtlist.Add("@Planid", planid);
                _srtlist.Add("@issetupfee", issetupfee);
                _srtlist.Add("@amount", amount);
                _srtlist.Add("@billingdays", billingdays);
                _srtlist.Add("@status", status);
                _srtlist.Add("@paymentlog", amount.ToString() + "$ " + paymentlog);
                _dt.executeNonQuery("addbilling_new", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }
        public bool Sendmail(string to, string body, string Subject = "Vacation Reward Codes", string from = "Claim Your Vacations", string yes = "Y")
        {
            bool str = true;
            try
            {
                string smtpEndpoint = "email-smtp.us-east-1.amazonaws.com";
                int port = 587;
                string senderName = "Travel Vacation Services";
                string senderAddress = "support@claimyourvacations.com";
                string toAddress = to;
                string smtpUsername = "AKIAYPH75NKRZXBWUIXR";
                string smtpPassword = "BARrLCnFb4yV8CydByjr87knWVmLga3O0SeBmc7mrRw8";
                string subject = "Get Your Complimentary Cruise and Vacation";
                AlternateView htmlBody = AlternateView.
                            CreateAlternateViewFromString(body, null, "text/html");
                MailMessage message = new MailMessage();
                message.From = new MailAddress(senderAddress, senderName);
                message.To.Add(new MailAddress(toAddress));
                message.Subject = subject;
                message.AlternateViews.Add(htmlBody);
                using (var client = new System.Net.Mail.SmtpClient(smtpEndpoint, port))
                {
                    client.Port = 587;
                    client.Host = "Smtpout.secureserver.net";
                    client.EnableSsl = true;
                    client.Timeout = 10000;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential("support@claimyourvacations.com", "RSS4y8R67hDXZWw");
                    try
                    {
                        client.Send(message);
                    }
                    // Show an error message if the message can't be sent
                    catch (Exception ex)
                    {
                        // new Comman().LogWrite(Enums.LogType.Error, ex.Message.ToString(), "Utilis", "Sendmail", Enums.Priority.Critical, ex.StackTrace);
                    }
                }

            }
            catch (Exception exception1)
            {
            }
            return str;

        }
        public List<string> updateBusinesslead(int lead)
        {
            List<string> _list = new List<string>();
            try
            {
                _list = getbusinesscodelist(1, lead);
                var str = String.Join(",", _list);
                updateleadstatus("Y", str, lead);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                //_dt.Dispose();
            }
            return _list;
        }
        public void updatestandardlead(int lead)
        {
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@userId", lead);
                _dt.executeNonQuery("Udt_std_dbusinessrewardCodes", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }
        public void addemail(string Fname, string Lname, string Email, string Mobile, string emailbody, string subject, string from)
        {
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@Email", Email);
                _srtlist.Add("@Fname", FirstCharToUpper(Fname));
                _srtlist.Add("@Lname", FirstCharToUpper(Lname));
                _srtlist.Add("@Mobile", Mobile);
                _srtlist.Add("@emailbody", emailbody);
                _srtlist.Add("@subject", subject);
                _srtlist.Add("@from", from);
                _dt.executeNonQuery("AddEmail", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }
        public List<string> getbusinesscodelist(int ordercount, int lead)
        {
            List<string> ipcount = new List<string>();
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@userid", lead);
                _srtlist.Add("@Ordercount", ordercount);
                var database = _dt.fillDataTable("UpdatebusinessrewardCodes", "", _srtlist);
                for (int i = 0; i < database.Rows.Count; i++)
                {
                    ipcount.Add(database.Rows[i][0].ToString());
                }
                database.Dispose();
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                return ipcount;
            }
            finally
            {
                _dt.Dispose();
            }
            return ipcount;
        }
        public void updateleadstatus(string status, string RewardCodes, int lead)
        {
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@status", status);
                _srtlist.Add("@RewardCodes", RewardCodes);
                _srtlist.Add("@userId", lead);
                _dt.executeNonQuery("UpdateStatuswithcode", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }
        [HttpPost]
        public ActionResult step1(formmodal _model)
        {
            
            Session["fullname"] = _model.firstname + " " + _model.lastname; 
            Session["lastname"] = _model.lastname;
            Session["firstname"] = _model.firstname;
            Session["cell"] = _model.cell;
            Session["email"] = _model.email; 
            Session["zipcode"] = _model.zip;  
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
             | SecurityProtocolType.Tls11
             | SecurityProtocolType.Tls12
             | SecurityProtocolType.Ssl3;
            string biltylink = "";
            bool isLinkValid = true;
            string utm_source = Session["utm_source"] == null ? "" : Session["utm_source"].ToString();
            try
            {
                string _res = "";
                var _data = IsBadIp(ref _res, Request.UserHostAddress, Request.UserAgent);
                if (_data.fraud_score > 50 ) 
                {
                    string responseString = "";
                    if (!string.IsNullOrEmpty(_model.cell))
                    {
                        var response = "https://getultimateoffer.com/V1/ismobile?Number=" + _model.cell.Replace('-', ' ').Replace(" ", "").Substring(0, 6);
                        if (_model.autoclick > 0)
                        {
                            responseString = "{'ResponseCode':' - 14','ResponseMeaning':'Duplicate Alert, Lead Is Exist in DB','ResponseText':'Duplicate Leads'}";
                        }
                        Session["response"] = responseString;
                        string id = new Helper().inserts2swithunid(_model, Request.UserHostAddress, responseString, false, utm_source, "", "", "SSO", _model.cell, Quality: (!isLinkValid ? "Link Data" : "Blocked IP"));
                        Session["firstname"] = FirstCharToUpper(_model.firstname);
                        Session["lastname"] = FirstCharToUpper(_model.lastname);
                        Session["email"] = _model.email;
                        Session["zipcode"] = _model.zip;
                        Session["cell"] = _model.cell;
                    } 
                    return RedirectToAction("step2");
                }
                else
                {
                    if (!string.IsNullOrEmpty(_model.cell))
                    {
                        string fbid = Session["fbid"] == null ? "" : Session["fbid"].ToString();
                        if (Session["formlead"] != null)
                        {
                            int leadid = Convert.ToInt32(Session["formlead"]);
                            updateformlead(leadid);
                            //updateleadpaymentpage(leadid);
                        }
                        else
                        {

                            if (!string.IsNullOrEmpty(_model.cell) && !string.IsNullOrEmpty(_model.email))
                            {
                                _model.firstname = FirstCharToUpper(_model.firstname);
                                isLinkValid = !string.IsNullOrEmpty(_model.firstname) ? isLinkValid : false;
                                _model.lastname = FirstCharToUpper(_model.lastname);
                                isLinkValid = (!string.IsNullOrEmpty(_model.lastname) || !_model.lastname.Contains("lastname")) ? isLinkValid : false;
                                _model.cell = _model.cell.Replace('-', ' ').Replace(" ", "");
                                isLinkValid = (!string.IsNullOrEmpty(_model.cell) || !_model.cell.Contains("number")) ? isLinkValid : false;
                                Session["fullname"] = _model.firstname.Replace(" ", "") + " " + _model.lastname.Replace(" ", "");
                                Session["lastname"] = _model.lastname.Replace(" ", "");
                                Session["firstname"] = _model.firstname.Replace(" ", "");
                                Session["cell"] = _model.cell;
                                Session["email"] = _model.email;
                                Session["zip"] = Session["zipcode"] == null ? "" : Session["zipcode"].ToString(); ;
                                isLinkValid = (!string.IsNullOrEmpty(_model.email) || !_model.email.Contains("email")) ? isLinkValid : false;
                                _model.zip = Session["zip"].ToString();
                                _model.dob = Session["dob"] == null ? "" : Session["dob"].ToString();// "07/18/2019"
                                _model.gender = Session["gender"] == null ? "" : Session["gender"].ToString();
                                _model.state = Session["state"] == null ? "" : Session["state"].ToString();
                                _model.city = Session["city"] == null ? "" : Session["city"].ToString();
                                _model.address = Session["address"] == null ? "" : Session["address"].ToString();
                                isLinkValid = !string.IsNullOrEmpty(_model.dob) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.gender) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.state) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.city) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.address) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.zip) ? isLinkValid : false;
                                var response = "https://getultimateoffer.com/V1/ismobile?Number=" + _model.cell.Replace('-', ' ').Replace(" ", "").Substring(0, 6);
                                string responseString = Get(response);
                                string unid = "";
                                string clickid = "";
                                if (Session["unid"] != null)
                                {
                                    unid = Convert.ToString(Session["unid"]);
                                    unid = unid.Replace('-', ' ').Replace(" ", "");
                                    if (unid.Contains("number") || string.IsNullOrEmpty(unid))
                                        isLinkValid = false;
                                }
                                else
                                {
                                    isLinkValid = false;
                                }
                                if (Session["utm_source"] != null)
                                {
                                    utm_source = Convert.ToString(Session["utm_source"]);
                                    if (string.IsNullOrEmpty(utm_source))
                                        isLinkValid = false;
                                }
                                else
                                {
                                    isLinkValid = false;
                                }
                                if (Session["clickid"] != null)
                                {
                                    clickid = Convert.ToString(Session["clickid"]);
                                    if (string.IsNullOrEmpty(clickid))
                                        isLinkValid = false;
                                }
                                else
                                {
                                    isLinkValid = false;
                                }
                                if (isunidExist(unid) > 0)
                                {
                                    responseString = "{'ResponseCode':' - 14','ResponseMeaning':'Duplicate Alert, Lead Is Exist in DB','ResponseText':'Duplicate Leads'}";
                                }
                                Session["response"] = responseString;
                                string id = new Helper().inserts2swithunid(_model, Request.UserHostAddress, responseString, false, utm_source, clickid, "", fbid, unid, Quality: isLinkValid ? "Link Data" : "Valid");
                                Session["formlead"] = id;
                                updateformlead(Convert.ToInt32(id));
                                //updateleadpaymentpage(leadid);
                            }
                        }
                        var newgender = "M";
                        biltylink = "https://spnccrzone.com/?eqi=0ugnf0Mj%2fEvAniErwl1pkKfOsoiKmWYblgDJwpjxrOw%3d&s1=" + utm_source + "&s2=";
                        if (!string.IsNullOrEmpty(_model.cell))
                        {
                            biltylink = biltylink + "&s3=" + _model.cell;
                            biltylink = biltylink + "&phonecode=" + _model.cell.Substring(0, 3) + "&phoneprefix=" + _model.cell.Substring(3, 3) + "&phonesuffix=" + _model.cell.Substring(6);
                            biltylink = biltylink + "&address2=United+States&telephone=" + _model.cell;
                        }
                        if (!string.IsNullOrEmpty(_model.firstname))
                        {
                            biltylink = biltylink + "&firstname=" + _model.firstname.Replace(" ", "");
                        }
                        if (!string.IsNullOrEmpty(_model.lastname))
                        {
                            biltylink = biltylink + "&lastname=" + _model.lastname.Replace(" ", "");
                        }
                        if (!string.IsNullOrEmpty(_model.email))
                        {
                            biltylink = biltylink + "&email=" + _model.email;
                        }
                        if (!string.IsNullOrEmpty(_model.zip))
                        {
                            biltylink = biltylink + "&zippost=" + _model.zip;
                        }
                        if (!string.IsNullOrEmpty(_model.address))
                        {
                            biltylink = biltylink + "&address1=" + _model.address;
                        }
                        if (!string.IsNullOrEmpty(_model.city))
                        {
                            biltylink = biltylink + "&city=" + _model.city;
                        }
                        if (!string.IsNullOrEmpty(_model.state))
                        {
                            biltylink = biltylink + "&state=" + _model.state;
                        }
                        if (!string.IsNullOrEmpty(_model.gender))
                        {
                            newgender = (_model.gender.ToLower() == "m" || _model.gender.ToLower() == "male") ? "M" : "F";
                            biltylink = biltylink + "&gender=" + newgender;
                        }
                        if (Session["dobmonth"] != null)
                        {
                            biltylink = biltylink + "&dobmonth=" + Session["dobmonth"].ToString();
                            biltylink = biltylink + "&dobday=" + Session["dobday"].ToString();
                            biltylink = biltylink + "&dobyear=" + Session["dobyear"].ToString();
                        }
                        insertpaymentlog(1, Convert.ToDouble("1"), 6, true, 30, "Link", biltylink, _model.email, _model.firstname.Replace(" ", "") + " " + _model.lastname.Replace(" ", ""), _model.cell);
                        ViewBag.click = biltylink;
                    }
                }
                return Redirect(biltylink);
            }
            catch (Exception ex)
            {
                return RedirectToAction("step2");
            }
        }
        public ActionResult step2()
        {
            if (Session["fullname"] == null)
            {
                return RedirectToAction("step1");
            }
            return View();
        }

        [HttpPost]
        public ActionResult step2(int count = 1, string ordercount = "1")
        {
            Session["count"] = count;
            Session["price"] = "$19.99";
            Session["ordercount"] = ordercount;
            return RedirectToAction("step3");
        }
        public ActionResult step3(int c = 0)
        {

            formmodal _model = new formmodal();
            _model.fullname = Session["fullname"].ToString();
            _model.address = Session["address"] != null ? Session["address"].ToString() : "";
            _model.city = Session["city"] != null ? Session["city"].ToString() : "";
            _model.state = Session["state"] != null ? Session["state"].ToString() : "";
            _model.zip = Session["zip"] != null ? Session["zip"].ToString() : "";
            _model.year = 2018;
            _model.month = 1;
            _model.type = "Visa";
            Session["url"] = "https://travelrewardsvacations.com/special/mobiles/step3";
            if (c == 1)
            {
                _model.cvv = Session["cvv"].ToString();
                _model.year = Convert.ToInt32(Session["exyear"].ToString());
                _model.month = Convert.ToInt32(Session["exdate"].ToString());
                _model.type = Session["ptype"].ToString();
                _model.cardnumber = Session["number"].ToString();
            }
            return View(_model);
        }
        public string Get(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}