﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class check_outController : Controller
    {

        public void CreateXMLEmailCustomCompaignReader(string value)
        {
            string empty = string.Empty;
            try
            {
                string str = string.Concat("<?xml version='1.0' encoding='utf-8' standalone='yes'?><Autosms><C1>", value, "</C1></Autosms>");
                if (System.IO.File.Exists(Server.MapPath("~/App_Data/EmailCustomCompaignReader.xml")))
                {
                    System.IO.File.Delete(Server.MapPath("~/App_Data/EmailCustomCompaignReader.xml"));
                }
                System.IO.File.WriteAllText(Server.MapPath("~/App_Data/EmailCustomCompaignReader.xml"), str);
            }
            catch (Exception exception)
            {
            }
        }

        [HttpPost]
        public ActionResult offer(formmodal model)
        {
            Session["userurl"] = Request.Url.ToString();
            string url = "https://getultimateoffer.com/sms/getdobbyfbid?mobile=" + model.cell + "&email=" + model.email;
            var res = Get(url);
            var res1 = JsonConvert.DeserializeObject<fbpromomodal>(res);
            if (!res1.dob.Contains("dob"))
            {
                Session["dob"] = res1.dob; // "07/18/2019"
                model.dob = Session["dob"].ToString();
                Session["userurl"] = Session["userurl"].ToString() + Request.Url.ToString() + "&dob=" + model.dob;
                Session["gender"] = res1.gender;
                model.gender = Session["gender"].ToString();
                Session["userurl"] = Session["userurl"].ToString() + "&gender=" + model.gender;
                if (!string.IsNullOrEmpty(res1.addr))
                {
                    Session["address"] = res1.addr;
                    model.address = res1.addr;
                    Session["userurl"] = Session["userurl"].ToString() + "&address=" + model.address;
                }
                if (!string.IsNullOrEmpty(res1.city))
                {
                    Session["city"] = res1.city;
                    model.city = res1.city;
                    Session["userurl"] = Session["userurl"].ToString() + "&city=" + model.city;
                }
                if (!string.IsNullOrEmpty(res1.state))
                {
                    Session["state"] = res1.state;
                    model.state = res1.state;
                    Session["userurl"] = Session["userurl"].ToString() + "&state=" + model.state;
                }
                if (!string.IsNullOrEmpty(res1.zipcode))
                {
                    Session["zipcode"] = res1.zipcode;
                    model.zip = res1.zipcode;
                    // Session["userurl"] = Session["userurl"].ToString() + "&state=" + _model.state;
                }
            }
            Session["firstname"] = model.firstname.Replace(" ", "");
            Session["lastname"] = model.lastname.Replace(" ", "");
            Session["cell"] = model.cell;
            Session["email"] = model.email;
            Session["zip"] = Session["zipcode"] == null ? "" : Session["zipcode"].ToString();
            model.dob = Session["dob"] == null ? "" : Session["dob"].ToString();// "07/18/2019"
            model.gender = Session["gender"] == null ? "" : Session["gender"].ToString();
            model.state = Session["state"] == null ? "" : Session["state"].ToString();
            model.city = Session["city"] == null ? "" : Session["city"].ToString();
            model.address = Session["address"] == null ? "" : Session["address"].ToString();
            string fbid = Session["fbid"] == null ? "" : Convert.ToString(Session["fbid"]);
            var querystring = "fbid=" + fbid + "&dob=" + model.dob + "&gender=" + model.gender + "&zip=" + model.zip + "&fname=" + model.firstname.Replace(" ", "") + "&lname=" + model.lastname.Replace(" ", "") + "&phone=" + model.cell + "&email=" + model.email + "&city=" + model.city + "&state=" + model.state + "&api_key=kghtyb30sumit56&travl=1" + "&addr=" + model.address;
            var response = "https://getultimateoffer.com/user/fbpromosend?" + querystring;
            string responseString = Get(response);
          string  utm_source = "";
            string  unid="";
            if (Session["utm_source"] != null)
            {
                utm_source = Convert.ToString(Session["utm_source"]); 
            }
            if(Session["unid"] !=null)
            {
                unid = Convert.ToString(Session["unid"]); 
            }
            Session["response"] = responseString;
            string id = inserts2slead(model, Request.UserHostAddress, responseString, true, utm_source, unid, "", fbid);
            Session["formlead"] = id;
            inserttgtlead(model, "https://ldsapi.tmginteractive.com/generateplacementscript.aspx", "40483000", "795054");
            return View(model);
        }

        public void updatewordinglead(int id)
        {
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@id", id);
                _dt.executeNonQuery("updateleadwording", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }

        [HttpPost]
        public ActionResult offer2(formmodal model)
        {
            if (Session["formlead"] != null)
            {
                int uid = Convert.ToInt32(Session["formlead"]);
                updatewordinglead(uid);
            }
            string url = "https://getultimateoffer.com/sms/getdobbyfbid?mobile=" + model.cell + "&email=" + model.email;
            var res = Get(url);
            var res1 = JsonConvert.DeserializeObject<fbpromomodal>(res); ;
            if (!res1.dob.Contains("dob"))
            {
                Session["dob"] = res1.dob; // "07/18/2019"
                model.dob = Session["dob"].ToString();
                Session["userurl"] = Session["userurl"].ToString() + Request.Url.ToString() + "&dob=" + model.dob;
                Session["gender"] = res1.gender;
                model.gender = Session["gender"].ToString();
                Session["userurl"] = Session["userurl"].ToString() + "&gender=" + model.gender;
                if (!string.IsNullOrEmpty(res1.addr))
                {
                    Session["address"] = res1.addr;
                    model.address = res1.addr;
                    Session["userurl"] = Session["userurl"].ToString() + "&address=" + model.address;
                }
                if (!string.IsNullOrEmpty(res1.city))
                {
                    Session["city"] = res1.city;
                    model.city = res1.city;
                    Session["userurl"] = Session["userurl"].ToString() + "&city=" + model.city;
                }
                if (!string.IsNullOrEmpty(res1.state))
                {
                    Session["state"] = res1.state;
                    model.state = res1.state;
                    Session["userurl"] = Session["userurl"].ToString() + "&state=" + model.state;
                }
                if (!string.IsNullOrEmpty(res1.zipcode))
                {
                    Session["zipcode"] = res1.zipcode;
                    model.zip = res1.zipcode;
                    ///Session["userurl"] = Session["userurl"].ToString() + "&state=" + _model.state;
                }
            }
            Session["firstname"] = model.firstname.Replace(" ", "");
            Session["lastname"] = model.lastname.Replace(" ", "");
            Session["cell"] = model.cell;
            Session["email"] = model.email;
            Session["zip"] = Session["zipcode"] == null ? "" : Session["zipcode"].ToString(); ;
            model.dob = Session["dob"] == null ? "" : Session["dob"].ToString();// "07/18/2019"
            model.gender = Session["gender"] == null ? "" : Session["gender"].ToString();
            model.state = Session["state"] == null ? "" : Session["state"].ToString();
            model.city = Session["city"] == null ? "" : Session["city"].ToString();
            model.address = Session["address"] == null ? "" : Session["address"].ToString();
            inserttgtlead(model, "https://ldsapi.tmginteractive.com/generateplacementscript.aspx", "40483000", "795054");
            return View(model);
        }

        public void inserttgtlead(formmodal model, string url = "", string placement = "", string publisher = "")
        {
            url = Session["userurl"] != null ? Session["userurl"].ToString() : url;
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@Address", model.address);
                _srtlist.Add("@City", model.city);
                _srtlist.Add("@email", model.email);
                _srtlist.Add("@Fname", model.firstname);
                _srtlist.Add("@Lname", model.lastname);
                _srtlist.Add("@MobileNumber", model.cell);
                _srtlist.Add("@State", model.state);
                _srtlist.Add("@ZipCode", model.zip);
                _srtlist.Add("@url", url);
                _srtlist.Add("@placement", placement);
                _srtlist.Add("@publisher", publisher);
                _srtlist.Add("@Dob", model.dob);
                _srtlist.Add("@gender", model.gender);
                _dt.executeNonQuery("addtgtlogs", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }


        [HttpPost]
        public ActionResult Index(formmodal model)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
             | SecurityProtocolType.Tls11
             | SecurityProtocolType.Tls12
             | SecurityProtocolType.Ssl3;
            model.firstname = Session["firstname"].ToString();
            model.lastname = Session["lastname"].ToString();
            model.cell = Session["cell"].ToString();
            model.email = Session["email"].ToString();
            model.zip = "";
            model.address = Session["address"] == null ? "" : Session["address"].ToString();
            model.dob = Session["dob"] == null ? "" : Session["dob"].ToString();
            model.gender = Session["gender"] == null ? "" : Session["gender"].ToString();
            model.state = Session["state"] == null ? "" : Session["state"].ToString();
            model.city = Session["city"] == null ? "" : Session["city"].ToString();
            bool sendsos = false;
            string s2sresponse = "";
            string responseString = "";
            string clickid = "";
            string utm_source = "";
            string subid = "";
            try
            {
                var querystring = "firstname=" + model.firstname.Replace(" ", "") + "&lastname=" + model.lastname.Replace(" ", "") + "&phone=" + model.cell + "&email=" + model.email + "&api_key=kghtyb30sumit56&travl=1";
                if (Session["clickid"] != null)
                {
                    clickid = Session["clickid"].ToString();
                    querystring = querystring + "&clickid=" + Convert.ToString(Session["clickid"]);
                }
                if (Session["affiliateid"] != null)
                {
                    querystring = querystring + "&affiliateid=" + Convert.ToString(Session["affiliateid"]);
                    subid = Session["affiliateid"].ToString();
                }
                if (Session["utm_source"] != null)
                {
                    utm_source = Convert.ToString(Session["utm_source"]);
                    querystring = querystring + "&utm_source=" + Convert.ToString(Session["utm_source"]);
                }
                if (Session["utm_medium"] != null)
                {
                    querystring = querystring + "&utm_medium=" + Convert.ToString(Session["utm_medium"]);
                }
               var response = "https://getultimateoffer.com/user/specialoffer?" + querystring;
                responseString = Session["response"] != null ? Session["response"].ToString() : "";
                if (responseString.Contains("Success"))
                {
                    sendsos = true;
                }
                sendsos = false;
            
                string id = inserts2slead(model, Request.UserHostAddress, s2sresponse, true, utm_source, clickid, "", subid);
                Session["leadid"] = id;
                //using (var client = new WebClient())
                //{
                //    var values = new NameValueCollection();
                //    values["phone"] = Session["cell"].ToString();
                //    values["email"] = Session["email"].ToString();
                //    values["type"] = "form";
                //    var response_new = client.UploadValues("https://getultimateoffer.com/sms/updatelead", values);
                //    var responseString_new = Encoding.Default.GetString(response_new);
                //}
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("index", "check_out");
        }

        public void updateformlead(int id)
        {
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@id", id);
                _dt.executeNonQuery("updatelogs2", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }

        public string inserts2sformlead(formmodal model, string host, string response = "", bool iss2spost = false, string utm_source = "", string clickid = "", string leadresponse = "", string subid = "")
        {
            string mes = "";
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@email", model.email);
                _srtlist.Add("@dob", model.dob);
                _srtlist.Add("@Fname", model.firstname.Replace(" ", ""));
                _srtlist.Add("@Lname", model.lastname.Replace(" ", ""));
                _srtlist.Add("@MobileNumber", model.cell);
                _srtlist.Add("@leadresponse", response);
                _srtlist.Add("@ispost", false);
                _srtlist.Add("@ip", host);
                _srtlist.Add("@utm_source", utm_source);
                _srtlist.Add("@clickid", clickid);
                mes = _dt.executeNonQueryWMessage("Addnews2slog4", "", _srtlist).ToString();
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
            return mes;
        }


        public string inserts2slead(formmodal model, string host, string response = "", bool iss2spost = false, string utm_source = "", string clickid = "", string leadresponse = "", string subid = "")
        {
            string mes = "";
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@email", model.email);
                _srtlist.Add("@dob", model.dob);
                _srtlist.Add("@Fname", model.firstname.Replace(" ", ""));
                _srtlist.Add("@Lname", model.lastname.Replace(" ", ""));
                _srtlist.Add("@MobileNumber", model.cell);
                _srtlist.Add("@Status", response);
                _srtlist.Add("@leadresponse", leadresponse);
                _srtlist.Add("@ispost", iss2spost);
                _srtlist.Add("@ip", host);
                _srtlist.Add("@utm_source", utm_source);
                _srtlist.Add("@clickid", clickid);
                _srtlist.Add("@subid", subid);

                mes = _dt.executeNonQueryWMessage("Addnews2slog3", "", _srtlist).ToString();
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
            return mes;
        }

        public string Get(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

       [Route("check_out/index/{id}")]
        public ActionResult Index(int c = 0)
        {
            Session["url"] = "https://vsgft.com/check_out/index";
            if (Session["firstname"] == null)
            {
                return Redirect("https://vsgft.com");
            }
            formmodal model = new Models.formmodal();
            model.address = Session["address"] != null ? Session["address"].ToString() : "";
            model.firstname = Session["firstname"].ToString();
            model.lastname = Session["lastname"].ToString();
            model.cell = Session["cell"].ToString();
            model.city = Session["city"] != null ? Session["city"].ToString() : "";
            model.state = Session["state"] != null ? Session["state"].ToString() : "";
            model.zip = Session["zip"] != null ? Session["zip"].ToString() : "";
            model.email = Session["email"].ToString();
            model.fullname = model.firstname + " " + model.lastname;
            model.year = 2018;
            model.month = 1;
            model.type = "Visa";
            string host = Request.UserHostAddress;
            if (c == 1)
            {
                model.cvv = Session["cvv"].ToString();
                model.year = Convert.ToInt32(Session["exyear"].ToString());
                model.month = Convert.ToInt32(Session["exdate"].ToString());
                model.type = Session["ptype"].ToString();
                model.cardnumber = Session["number"].ToString();
            }
            return View(model);
        }
    }
}