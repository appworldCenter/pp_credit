﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.utility;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public class Root
        {
            public bool success { get; set; }
            public string message { get; set; }
            public int fraud_score { get; set; }
            public string country_code { get; set; }
            public string region { get; set; }
            public string city { get; set; }
            public string ISP { get; set; }
            public int ASN { get; set; }
            public string operating_system { get; set; }
            public string browser { get; set; }
            public string organization { get; set; }
            public bool is_crawler { get; set; }
            public string timezone { get; set; }
            public bool mobile { get; set; }
            public string host { get; set; }
            public bool proxy { get; set; }
            public bool vpn { get; set; }
            public bool tor { get; set; }
            public bool active_vpn { get; set; }
            public bool active_tor { get; set; }
            public string device_brand { get; set; }
            public string device_model { get; set; }
            public bool recent_abuse { get; set; }
            public bool bot_status { get; set; }
            public string connection_type { get; set; }
            public string abuse_velocity { get; set; }
            public string zip_code { get; set; }
            public double latitude { get; set; }
            public double longitude { get; set; }
            public string request_id { get; set; }
        }
        public string Get(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
        public void SendnotifyAsync()
        {
            string firstname = Session["firstname"] == null ? "" : Session["firstname"].ToString();
            string lastname = Session["lastname"] == null ? "" : Session["lastname"].ToString();
            string phone = Session["phone"] == null ? "" : Session["phone"].ToString();
            string email = Session["email"] == null ? "" : Session["email"].ToString();
            string c = Session["c"] == null ? "" : Session["c"].ToString();
            string alerturl = "https://getultimateoffer.com/user/getalertmessage?fname=" + firstname + "&lname=" + lastname + "&phone=" + phone + "&email=" + email + "&c=" + c;
            if (!string.IsNullOrEmpty(firstname))
            {
                Get(alerturl);
            }
        }

        public void SendvisitAsync()
        {
            bool isLinkValid = true;
            formmodal _model = new formmodal();
            //_model.firstname = Session["fullname"] == null ? "" : Session["fullname"].ToString();
            _model.lastname = Session["lastname"] == null ? "" : Session["lastname"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.lastname) || !_model.lastname.Contains("lastname")) ? isLinkValid : false;
            _model.firstname = Session["firstname"] == null ? "" : Session["firstname"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.firstname) || !_model.firstname.Contains("firstname")) ? isLinkValid : false;
            _model.cell = Session["cell"] == null ? "" : Session["cell"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.cell) || !_model.cell.Contains("number")) ? isLinkValid : false;
            _model.email = Session["email"] == null ? "" : Session["email"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.email) || !_model.email.Contains("email")) ? isLinkValid : false;
            _model.dob = Session["dob"] == null ? "" : Session["dob"].ToString();// "07/18/2019"
            isLinkValid = (!string.IsNullOrEmpty(_model.dob)) ? isLinkValid : false;
            _model.gender = Session["gender"] == null ? "" : Session["gender"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.gender)) ? isLinkValid : false;
            _model.state = Session["state"] == null ? "" : Session["state"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.state)) ? isLinkValid : false;
            _model.city = Session["city"] == null ? "" : Session["city"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.city)) ? isLinkValid : false;
            _model.address = Session["address"] == null ? "" : Session["address"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.address)) ? isLinkValid : false;
            _model.zip = Session["zipcode"] == null ? _model.zip : Session["zipcode"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.zip)) ? isLinkValid : false;
            string fbid = Session["fbid"] == null ? "" : Convert.ToString(Session["fbid"]);
            var querystring = "fbid=" + fbid + "&dob=" + _model.dob + "&gender=" + _model.gender + "&zip=" + _model.zip + "&fname=" + _model.firstname.Replace(" ", "") + "&lname=" + _model.lastname.Replace(" ", "") + "&phone=" + _model.cell + "&email=" + _model.email + "&city=" + _model.city + "&state=" + _model.state + "&api_key=kghtyb30sumit56&travl=1" + "&addr=" + _model.address;
            string responseString = "{'ResponseCode':' - 1','ResponseMeaning':'Invalid data','ResponseText':'" + querystring + "'}";
            if (!string.IsNullOrEmpty(_model.cell))
            {
                var response = "https://getultimateoffer.com/V1/ismobile?Number=" + _model.cell.Replace('-', ' ').Replace(" ", "").Substring(0, 6);
                responseString = Get(response);
                _model.cell = _model.cell.Replace('-', ' ').Replace(" ", "");
            }
            string utm_source = "";
            string unid = "";
            string clickid = "";
            if (Session["unid"] != null)
            {
                unid = Convert.ToString(Session["unid"]);
                unid = unid.Replace('-', ' ').Replace(" ", "");
            }
            isLinkValid = (!string.IsNullOrEmpty(unid) || !unid.Contains("number")) ? isLinkValid : false;
            if (Session["utm_source"] != null)
            {
                utm_source = Convert.ToString(Session["utm_source"]);
            }
            isLinkValid = (!string.IsNullOrEmpty(utm_source) || !utm_source.Contains("number")) ? isLinkValid : false;
            if (Session["clickid"] != null)
            {
                clickid = Convert.ToString(Session["clickid"]);
            }
            isLinkValid = (!string.IsNullOrEmpty(clickid) || !clickid.Contains("number")) ? isLinkValid : false;
            if (!string.IsNullOrEmpty(unid))
            {
                if (isunidExist(unid) > 0)
                {
                    responseString = "{'ResponseCode':' - 14','ResponseMeaning':'Duplicate Alert, Lead Is Exist in DB','ResponseText':'Duplicate Leads'}";
                }
            }
            Session["response"] = responseString;
            //string EmailVerifyResponse = "";
            //int Isverified = verifyByOne(ref EmailVerifyResponse, _model.email)==true?1:0;
            string id = new Helper().inserts2swithunid(_model, Request.UserHostAddress, responseString, false, utm_source, clickid, "", fbid, unid, Quality: isLinkValid ? "Link Data" : "Valid");
            Session["formlead"] = id;
            if (responseString.ToLower().Contains("success"))
            {
                var values = new NameValueCollection();
                values["dobday"] = Session["dobday"] == null ? "01" : Session["dobday"].ToString();
                values["dobmonth"] = Session["dobmonth"] == null ? "01" : Session["dobmonth"].ToString();
                values["phone"] = _model.cell;
                values["fname"] = FirstCharToUpper(_model.firstname);
                values["lname"] = FirstCharToUpper(_model.lastname);
                values["email"] = _model.email;
                values["city"] = _model.city;
                values["state"] = _model.state;
                values["zip"] = _model.zip;
                values["subid"] = fbid;
                values["unid"] = unid;
                values["street"] = _model.address;
                values["gender"] = _model.gender;
                values["dobyear"] = Session["dobyear"] == null ? "1990" : Session["dobyear"].ToString();
                values["leadid"] = clickid;
                Post("https://getultimateoffer.com/v1/AddSpecialSMS", values);
            }
        }
        public string Post(string uri, NameValueCollection collections)
        {
            string message = "";
            using (var client = new WebClient())
            {
                var response2 = client.UploadValues(uri, collections);
                message = System.Text.Encoding.Default.GetString(response2);
            }
            return message;
        }
        public int isunidExist(string unid = "")
        {
            int isexist = 0;
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@unid", unid);
                var _count = _dt.executeNonQueryWMessage("checkunid", "", _srtlist).ToString();
                isexist = Convert.ToInt16(_count);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
            return isexist;
        }
        public string FirstCharToUpper(string input)
        {
            return string.IsNullOrEmpty(input) ? "" : input.First().ToString().ToUpper() + input.Substring(1);
        }
        public Root IsBadIp(ref string _response, string ip = "", string user_agent = "")
        {
            ip = string.IsNullOrEmpty(ip) ? Request.UserHostAddress : ip;
            user_agent = string.IsNullOrEmpty(user_agent) ? Request.UserAgent : user_agent;
            var url = $"https://ipqualityscore.com/api/json/ip/L8A7fsYYUIrM8iPW19Xq9iIKGhMoXKMe/{ip}?strictness=0&allow_public_access_points=true&user_agent={user_agent}&user_language=en-US&campaignID=VSGFT";
            _response = Get(url);
            var data = JsonConvert.DeserializeObject<Root>(_response);
            _response += " Url: " + url;
            return data;
        }

        [HttpPost]
        public string step1offer(formmodal _model)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
             | SecurityProtocolType.Tls11
             | SecurityProtocolType.Tls12
             | SecurityProtocolType.Ssl3;
            string biltylink = "";
            bool isLinkValid = true;
            string utm_source = Session["utm_source"] == null ? "" : Session["utm_source"].ToString();
            try
            {
                string _res = "";
                bool v = false;
                var _data = IsBadIp(ref _res, Request.UserHostAddress, Request.UserAgent);
                if (v)
                {
                    string responseString = "";
                    if (!string.IsNullOrEmpty(_model.cell))
                    {
                        var response = "https://getultimateoffer.com/V1/ismobile?Number=" + _model.cell.Replace('-', ' ').Replace(" ", "").Substring(0, 6);
                        if (_model.autoclick > 0)
                        {
                            responseString = "{'ResponseCode':' - 14','ResponseMeaning':'Duplicate Alert, Lead Is Exist in DB','ResponseText':'Duplicate Leads'}";
                        }
                        Session["response"] = responseString;
                        string id = new Helper().inserts2swithunid(_model, Request.UserHostAddress, responseString, false, utm_source, "", "", "SSO", _model.cell, Quality: (!isLinkValid ? "Link Data" : "Blocked IP"));
                        Session["firstname"] = FirstCharToUpper(_model.firstname);
                        Session["lastname"] = FirstCharToUpper(_model.lastname);
                        Session["email"] = _model.email;
                        Session["zipcode"] = _model.zip;
                        Session["cell"] = _model.cell;
                    }
                    biltylink = "disabled"; //return View("", _model);
                }
                else
                {
                    biltylink = "https://spnccrzone.com/?eqi=0ugnf0Mj%2fEvAniErwl1pkKfOsoiKmWYblgDJwpjxrOw%3d&s1=SSO&s2=";
                    if (!string.IsNullOrEmpty(_model.cell))
                    {
                        string fbid = Session["fbid"] == null ? "" : Session["fbid"].ToString();
                        if (Session["formlead"] != null)
                        {
                            int leadid = Convert.ToInt32(Session["formlead"]);
                            updateformlead(leadid);
                            //updateleadpaymentpage(leadid);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_model.cell) && !string.IsNullOrEmpty(_model.email))
                            {
                                _model.firstname = FirstCharToUpper(_model.firstname);
                                isLinkValid = !string.IsNullOrEmpty(_model.firstname) ? isLinkValid : false;
                                _model.lastname = FirstCharToUpper(_model.lastname);
                                isLinkValid = (!string.IsNullOrEmpty(_model.lastname) || !_model.lastname.Contains("lastname")) ? isLinkValid : false;
                                _model.cell = _model.cell.Replace('-', ' ').Replace(" ", "");
                                isLinkValid = (!string.IsNullOrEmpty(_model.cell) || !_model.cell.Contains("number")) ? isLinkValid : false;
                                Session["fullname"] = _model.firstname.Replace(" ", "") + " " + _model.lastname.Replace(" ", "");
                                Session["lastname"] = _model.lastname.Replace(" ", "");
                                Session["firstname"] = _model.firstname.Replace(" ", "");
                                Session["cell"] = _model.cell;
                                Session["email"] = _model.email;
                                Session["zip"] = Session["zipcode"] == null ? "" : Session["zipcode"].ToString(); ;
                                isLinkValid = (!string.IsNullOrEmpty(_model.email) || !_model.email.Contains("email")) ? isLinkValid : false;
                                _model.zip = Session["zip"].ToString();
                                _model.dob = Session["dob"] == null ? "" : Session["dob"].ToString();// "07/18/2019"
                                _model.gender = Session["gender"] == null ? "" : Session["gender"].ToString();
                                _model.state = Session["state"] == null ? "" : Session["state"].ToString();
                                _model.city = Session["city"] == null ? "" : Session["city"].ToString();
                                _model.address = Session["address"] == null ? "" : Session["address"].ToString();
                                isLinkValid = !string.IsNullOrEmpty(_model.dob) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.gender) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.state) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.city) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.address) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.zip) ? isLinkValid : false;
                                var response = "https://getultimateoffer.com/V1/ismobile?Number=" + _model.cell.Replace('-', ' ').Replace(" ", "").Substring(0, 6);
                                string responseString = Get(response);
                                string unid = "";
                                string clickid = "";
                                if (Session["unid"] != null)
                                {
                                    unid = Convert.ToString(Session["unid"]);
                                    unid = unid.Replace('-', ' ').Replace(" ", "");
                                    if (unid.Contains("number") || string.IsNullOrEmpty(unid))
                                        isLinkValid = false;
                                }
                                else
                                {
                                    isLinkValid = false;
                                }
                                if (Session["utm_source"] != null)
                                {
                                    utm_source = Convert.ToString(Session["utm_source"]);
                                    if (string.IsNullOrEmpty(utm_source))
                                        isLinkValid = false;
                                }
                                else
                                {
                                    isLinkValid = false;
                                }
                                if (Session["clickid"] != null)
                                {
                                    clickid = Convert.ToString(Session["clickid"]);
                                    if (string.IsNullOrEmpty(clickid))
                                        isLinkValid = false;
                                }
                                else
                                {
                                    isLinkValid = false;
                                }
                                if (isunidExist(unid) > 0)
                                {
                                    responseString = "{'ResponseCode':' - 14','ResponseMeaning':'Duplicate Alert, Lead Is Exist in DB','ResponseText':'Duplicate Leads'}";
                                }
                                Session["response"] = responseString;
                                string id = new Helper().inserts2swithunid(_model, Request.UserHostAddress, responseString, false, utm_source, clickid, "", fbid, unid, Quality: isLinkValid ? "Link Data" : "Valid");
                                Session["formlead"] = id;
                                updateformlead(Convert.ToInt32(id));
                                //updateleadpaymentpage(leadid);
                            }
                        }
                        var newgender = "M";
                        if (!string.IsNullOrEmpty(_model.cell))
                        {
                            biltylink = biltylink + "&s3=" + _model.cell;
                            biltylink = biltylink + "&phonecode=" + _model.cell.Substring(0, 3) + "&phoneprefix=" + _model.cell.Substring(3, 3) + "&phonesuffix=" + _model.cell.Substring(6);
                            biltylink = biltylink + "&address2=United+States&telephone=" + _model.cell;
                        }
                        if (!string.IsNullOrEmpty(_model.firstname))
                        {
                            biltylink = biltylink + "&firstname=" + _model.firstname.Replace(" ", "");
                        }
                        if (!string.IsNullOrEmpty(_model.lastname))
                        {
                            biltylink = biltylink + "&lastname=" + _model.lastname.Replace(" ", "");
                        }
                        if (!string.IsNullOrEmpty(_model.email))
                        {
                            biltylink = biltylink + "&email=" + _model.email;
                        }
                        if (!string.IsNullOrEmpty(_model.zip))
                        {
                            biltylink = biltylink + "&zippost=" + _model.zip;
                        }
                        if (!string.IsNullOrEmpty(_model.address))
                        {
                            biltylink = biltylink + "&address1=" + _model.address;
                        }
                        if (!string.IsNullOrEmpty(_model.city))
                        {
                            biltylink = biltylink + "&city=" + _model.city;
                        }
                        if (!string.IsNullOrEmpty(_model.state))
                        {
                            biltylink = biltylink + "&state=" + _model.state;
                        }
                        if (!string.IsNullOrEmpty(_model.gender))
                        {
                            newgender = (_model.gender.ToLower() == "m" || _model.gender.ToLower() == "male") ? "M" : "F";
                            biltylink = biltylink + "&gender=" + newgender;
                        }
                        if (Session["dobmonth"] != null)
                        {
                            biltylink = biltylink + "&dobmonth=" + Session["dobmonth"].ToString();
                            biltylink = biltylink + "&dobday=" + Session["dobday"].ToString();
                            biltylink = biltylink + "&dobyear=" + Session["dobyear"].ToString();
                        }
                        insertpaymentlog(1, Convert.ToDouble("1"), 6, true, 30, "Link", biltylink, _model.email, _model.firstname.Replace(" ", "") + " " + _model.lastname.Replace(" ", ""), _model.cell);
                        //ViewBag.click = biltylink;
                    }
                }
                return biltylink;
            }
            catch (Exception ex)
            {
                return ex.Message + " " + ex.StackTrace;
            }

        }

        public string getoffer(formmodal _model)
        {
            string biltylink = "";
            bool isLinkValid = true;
            Session["firstname"] = FirstCharToUpper(_model.firstname);
            Session["lastname"] = FirstCharToUpper(_model.lastname);
            Session["email"] = _model.email;
            Session["zipcode"] = _model.zip;
            Session["cell"] = _model.cell;
            string utm_source = Session["utm_source"] == null ? "" : Session["utm_source"].ToString();
            try
            {
                string _res = "";
                var _data = IsBadIp(ref _res, (_model.firstname == "Analida" ? "35.141.9.79" : Request.UserHostAddress), Request.UserAgent);
                if (_data.fraud_score > 50)
                {
                    string responseString = "";
                    if (!string.IsNullOrEmpty(_model.cell))
                    {
                        var response = "https://getultimateoffer.com/V1/ismobile?Number=" + _model.cell.Replace('-', ' ').Replace(" ", "").Substring(0, 6);
                        if (_model.autoclick > 0)
                        {
                            responseString = "{'ResponseCode':' - 14','ResponseMeaning':'Duplicate Alert, Lead Is Exist in DB','ResponseText':'Duplicate Leads'}";
                        }
                        Session["response"] = responseString;
                        string id = new Helper().inserts2swithunid(_model, Request.UserHostAddress, responseString, false, utm_source, "", "", "SSO", _model.cell, Quality: (!isLinkValid ? "Link Data" : "Blocked IP"));

                    }
                    biltylink = "disabled"; //return View("", _model);
                }
                else
                {
                    biltylink = "https://spnccrzone.com/?eqi=0ugnf0Mj%2fEvAniErwl1pkKfOsoiKmWYblgDJwpjxrOw%3d&s1=SSO&s2=";
                    if (!string.IsNullOrEmpty(_model.cell))
                    {
                        string fbid = Session["fbid"] == null ? "" : Session["fbid"].ToString();
                        if (Session["formlead"] != null)
                        {
                            int leadid = Convert.ToInt32(Session["formlead"]);
                            updateformlead(leadid);
                            //updateleadpaymentpage(leadid);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_model.cell) && !string.IsNullOrEmpty(_model.email))
                            {
                                _model.firstname = FirstCharToUpper(_model.firstname);
                                isLinkValid = !string.IsNullOrEmpty(_model.firstname) ? isLinkValid : false;
                                _model.lastname = FirstCharToUpper(_model.lastname);
                                isLinkValid = (!string.IsNullOrEmpty(_model.lastname) || !_model.lastname.Contains("lastname")) ? isLinkValid : false;
                                _model.cell = _model.cell.Replace('-', ' ').Replace(" ", "");
                                isLinkValid = (!string.IsNullOrEmpty(_model.cell) || !_model.cell.Contains("number")) ? isLinkValid : false;
                                Session["fullname"] = _model.firstname.Replace(" ", "") + " " + _model.lastname.Replace(" ", "");
                                Session["lastname"] = _model.lastname.Replace(" ", "");
                                Session["firstname"] = _model.firstname.Replace(" ", "");
                                Session["cell"] = _model.cell;
                                Session["email"] = _model.email;
                                Session["zip"] = Session["zipcode"] == null ? "" : Session["zipcode"].ToString(); ;
                                isLinkValid = (!string.IsNullOrEmpty(_model.email) || !_model.email.Contains("email")) ? isLinkValid : false;
                                _model.zip = Session["zip"].ToString();
                                _model.dob = Session["dob"] == null ? "" : Session["dob"].ToString();// "07/18/2019"
                                _model.gender = Session["gender"] == null ? "" : Session["gender"].ToString();
                                _model.state = Session["state"] == null ? "" : Session["state"].ToString();
                                _model.city = Session["city"] == null ? "" : Session["city"].ToString();
                                // _model.address = Session["address"] == null ? "" : Session["address"].ToString();
                                isLinkValid = !string.IsNullOrEmpty(_model.dob) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.gender) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.state) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.city) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.address) ? isLinkValid : false;
                                isLinkValid = !string.IsNullOrEmpty(_model.zip) ? isLinkValid : false;
                                var response = "https://getultimateoffer.com/V1/ismobile?Number=" + _model.cell.Replace('-', ' ').Replace(" ", "").Substring(0, 6);
                                string responseString = Get(response);
                                string unid = "";
                                string clickid = "";
                                if (Session["unid"] != null)
                                {
                                    unid = Convert.ToString(Session["unid"]);
                                    unid = unid.Replace('-', ' ').Replace(" ", "");
                                    if (unid.Contains("number") || string.IsNullOrEmpty(unid))
                                        isLinkValid = false;
                                }
                                else
                                {
                                    isLinkValid = false;
                                }
                                if (Session["utm_source"] != null)
                                {
                                    utm_source = Convert.ToString(Session["utm_source"]);
                                    if (string.IsNullOrEmpty(utm_source))
                                        isLinkValid = false;
                                }
                                else
                                {
                                    isLinkValid = false;
                                }
                                if (Session["clickid"] != null)
                                {
                                    clickid = Convert.ToString(Session["clickid"]);
                                    if (string.IsNullOrEmpty(clickid))
                                        isLinkValid = false;
                                }
                                else
                                {
                                    isLinkValid = false;
                                }
                                if (isunidExist(unid) > 0)
                                {
                                    responseString = "{'ResponseCode':' - 14','ResponseMeaning':'Duplicate Alert, Lead Is Exist in DB','ResponseText':'Duplicate Leads'}";
                                }
                                Session["response"] = responseString;
                                string id = new Helper().inserts2swithunid(_model, Request.UserHostAddress, responseString, false, utm_source, clickid, "", fbid, unid, Quality: isLinkValid ? "Link Data" : "Valid");
                                Session["formlead"] = id;
                                updateformlead(Convert.ToInt32(id));
                                //updateleadpaymentpage(leadid);
                            }
                        }
                        var newgender = "M";

                        if (!string.IsNullOrEmpty(_model.cell))
                        {
                            biltylink = biltylink + "&s3=" + _model.cell;
                            biltylink = biltylink + "&phonecode=" + _model.cell.Substring(0, 3) + "&phoneprefix=" + _model.cell.Substring(3, 3) + "&phonesuffix=" + _model.cell.Substring(6);
                            biltylink = biltylink + "&address2=United+States&telephone=" + _model.cell;
                        }
                        if (!string.IsNullOrEmpty(_model.firstname))
                        {
                            biltylink = biltylink + "&firstname=" + _model.firstname.Replace(" ", "");
                        }
                        if (!string.IsNullOrEmpty(_model.lastname))
                        {
                            biltylink = biltylink + "&lastname=" + _model.lastname.Replace(" ", "");
                        }
                        if (!string.IsNullOrEmpty(_model.email))
                        {
                            biltylink = biltylink + "&email=" + _model.email;
                        }
                        if (!string.IsNullOrEmpty(_model.zip))
                        {
                            biltylink = biltylink + "&zippost=" + _model.zip;
                        }
                        if (!string.IsNullOrEmpty(_model.address))
                        {
                            biltylink = biltylink + "&address1=" + _model.address;
                        }
                        if (!string.IsNullOrEmpty(_model.city))
                        {
                            biltylink = biltylink + "&city=" + _model.city;
                        }
                        if (!string.IsNullOrEmpty(_model.state))
                        {
                            biltylink = biltylink + "&state=" + _model.state;
                        }
                        if (!string.IsNullOrEmpty(_model.gender))
                        {
                            newgender = (_model.gender.ToLower() == "m" || _model.gender.ToLower() == "male") ? "M" : "F";
                            biltylink = biltylink + "&gender=" + newgender;
                        }
                        if (Session["dobmonth"] != null)
                        {
                            biltylink = biltylink + "&dobmonth=" + Session["dobmonth"].ToString();
                            biltylink = biltylink + "&dobday=" + Session["dobday"].ToString();
                            biltylink = biltylink + "&dobyear=" + Session["dobyear"].ToString();
                        }
                        insertpaymentlog(1, Convert.ToDouble("1"), 6, true, 30, "Link", biltylink, _model.email, _model.firstname.Replace(" ", "") + " " + _model.lastname.Replace(" ", ""), _model.cell);
                        ViewBag.click = biltylink;
                    }
                }
                return biltylink;
            }
            catch (Exception ex)
            {
                return ex.Message + " " + ex.StackTrace;
            }

        }
        public void updateformlead(int id)
        {
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@id", id);
                _dt.executeNonQuery("updatelogs", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }
        public void insertpaymentlog(int userId, double amount, int planid, bool issetupfee, int billingdays, string status, string paymentlog, string email = "", string name = "", string mobile = "")
        {
            SqlHelper _dt = new SqlHelper();
            var clickid = "";
            var subid = Session["fbid"] == null ? "flnt2" : Convert.ToString(Session["fbid"]);
            var ip = Request.UserHostAddress;
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@mobile", mobile);
                _srtlist.Add("@name", name);
                _srtlist.Add("@email", email);
                _srtlist.Add("@clickid", clickid);
                _srtlist.Add("@subid", subid);
                _srtlist.Add("@ip", ip);
                _srtlist.Add("@userId", userId);
                _srtlist.Add("@Planid", planid);
                _srtlist.Add("@issetupfee", issetupfee);
                _srtlist.Add("@amount", amount);
                _srtlist.Add("@billingdays", billingdays);
                _srtlist.Add("@status", status);
                _srtlist.Add("@paymentlog", paymentlog);
                _dt.executeNonQuery("addbilling", "", _srtlist);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                _dt.Dispose();
            }
        }
        [Route("Home/index/{id}")]
        public ActionResult Index(string id, string type = "1")
        {
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                return Redirect(redirectUrl);
            }
            formmodal _model = new formmodal();
            var r = Request.FilePath;
            int isshorturl = 0;
            ViewBag.Title = "Home Page";
            string path = r.Replace("/", "");
            ViewBag.path = "";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
         | SecurityProtocolType.Tls11
         | SecurityProtocolType.Tls12
         | SecurityProtocolType.Ssl3;
            if (!string.IsNullOrEmpty(path))
            {
                string url = getlongurl(path);
                var uri = new Uri(url);
                var query = HttpUtility.ParseQueryString(uri.Query);
                isshorturl = 1;
                Session["utm_source"] = query.Get("utm_source") == null ? "" : query.Get("utm_source");
                Session["clickid"] = query.Get("leadid") == null ? "" : query.Get("leadid");
                string unid = query.Get("unid") == null ? "" : query.Get("unid");
                unid = unid.Replace('-', ' ').Replace(" ", "");
                Session["unid"] = unid;
                int c = isunidExist(unid);
                string phone = query.Get("phone") == null ? "" : query.Get("phone");
                _model.cell = phone.Replace('-', ' ').Replace(" ", "");
                Session["cell"] = phone; Session["phone"] = phone;
                c = isunidExist(phone);
                Session["c"] = c == 0 ? "" : c.ToString();
                _model.autoclick = c;
                string firstname = query.Get("firstname") == null ? "" : query.Get("firstname");
                Session["firstname"] = FirstCharToUpper(firstname);
                _model.firstname = FirstCharToUpper(firstname);
                string lastname = query.Get("lastname") == null ? "" : query.Get("lastname");
                Session["lastname"] = FirstCharToUpper(lastname);
                _model.lastname = FirstCharToUpper(lastname);
                string email = query.Get("email") == null ? "" : query.Get("email");
                Session["email"] = email;
                _model.email = email;
                string zipcode = query.Get("zipcode") == null ? "" : query.Get("zipcode");
                Session["zipcode"] = zipcode;
                _model.zip = zipcode;
                string state = query.Get("state") == null ? "" : query.Get("state");
                Session["state"] = state.ToUpper();
                _model.state = state.ToUpper();
                string city = query.Get("city") == null ? "" : query.Get("city");
                Session["city"] = city;
                _model.city = city;
                var subid = query.Get("subid") == null ? "" : query.Get("subid");
                Session["fbid"] = subid;
                var dobmonth = query.Get("dobmonth") == null ? "" : query.Get("dobmonth");
                var dobday = query.Get("dobday") == null ? "" : query.Get("dobday");
                var dobyear = query.Get("dobyear") == null ? "" : query.Get("dobyear");
                if (!string.IsNullOrEmpty(dobyear))
                {
                    Session["dobmonth"] = dobmonth;
                    Session["dobday"] = dobday;
                    Session["dobyear"] = dobyear;
                    Session["dob"] = dobday + "/" + dobmonth + "/" + dobyear;
                }
                string gender = query.Get("gender") == null ? "" : query.Get("gender");
                Session["gender"] = gender;
                _model.gender = gender;
                string street = query.Get("street") == null ? "" : query.Get("street");
                Session["address"] = street.Contains(',')? street.Substring(0, street.IndexOf(',')): street;
                _model.address = street.Contains(',') ? street.Substring(0, street.IndexOf(',')) : street;
            }
            var _d = JsonConvert.SerializeObject(_model);
            if (System.IO.File.Exists(Server.MapPath("~/App_Data/autores.xml")))
            {
                System.IO.File.Delete(Server.MapPath("~/App_Data/autores.xml"));
            }
            System.IO.File.WriteAllText(Server.MapPath("~/App_Data/autores.xml"), _d);
            ViewBag.isshorturl = isshorturl;
            var _url = "disabled";
            if (!string.IsNullOrEmpty(_model.firstname) && !string.IsNullOrEmpty(_model.firstname))
            {
                _url = getoffer(_model);
            }
            if (_url == "disabled")
            {
                string view = "index";
                if (Request.Browser.IsMobileDevice == true)
                {
                    view = "mobile";
                    return View(view, _model);
                }
                else
                {
                    return View(view, _model);
                }
            }
            else
            {
                ViewBag.url = _url;
                return View("redirect", _model);
            }

        }
        public string getlongurl(string urlId)
        {
            string message = "";
            SqlHelperold sqlHelper = new SqlHelperold();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@urlId", urlId);
                _srt.Add("@ip", Request.UserHostAddress);
                message = sqlHelper.executeNonQueryWMessage("GetUrl", "", _srt).ToString() + "&urlId=" + urlId;

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
                message = exception.Message;
            }
            finally
            {
                sqlHelper.Dispose();

            }
            return message;
        }
    }
}
