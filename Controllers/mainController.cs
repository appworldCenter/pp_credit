﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class mainController : Controller
    {
        public class Root
        {
            public bool success { get; set; }
            public string message { get; set; }
            public int fraud_score { get; set; }
            public string country_code { get; set; }
            public string region { get; set; }
            public string city { get; set; }
            public string ISP { get; set; }
            public int ASN { get; set; }
            public string operating_system { get; set; }
            public string browser { get; set; }
            public string organization { get; set; }
            public bool is_crawler { get; set; }
            public string timezone { get; set; }
            public bool mobile { get; set; }
            public string host { get; set; }
            public bool proxy { get; set; }
            public bool vpn { get; set; }
            public bool tor { get; set; }
            public bool active_vpn { get; set; }
            public bool active_tor { get; set; }
            public string device_brand { get; set; }
            public string device_model { get; set; }
            public bool recent_abuse { get; set; }
            public bool bot_status { get; set; }
            public string connection_type { get; set; }
            public string abuse_velocity { get; set; }
            public string zip_code { get; set; }
            public double latitude { get; set; }
            public double longitude { get; set; }
            public string request_id { get; set; }
        }
        public string FirstCharToUpper(string input)
        {
            return string.IsNullOrEmpty(input) ? "" : input.First().ToString().ToUpper() + input.Substring(1);
        }
        public void SendnotifyAsync()
        {
            string firstname = Session["firstname"] == null ? "" : Session["firstname"].ToString();
            string lastname = Session["lastname"] == null ? "" : Session["lastname"].ToString();
            string phone = Session["phone"] == null ? "" : Session["phone"].ToString();
            string email = Session["email"] == null ? "" : Session["email"].ToString();
            string c = Session["c"] == null ? "" : Session["c"].ToString();
            string alerturl = "https://getultimateoffer.com/user/getalertmessage?fname=" + firstname + "&lname=" + lastname + "&phone=" + phone + "&email=" + email + "&c=" + c;
            if (!string.IsNullOrEmpty(firstname))
            {
                Get(alerturl);
            }
        }

        public void SendvisitAsync()
        {
            bool isLinkValid = true;
            formmodal _model = new formmodal();
            //_model.firstname = Session["fullname"] == null ? "" : Session["fullname"].ToString();
            _model.lastname = Session["lastname"] == null ? "" : Session["lastname"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.lastname) || !_model.lastname.Contains("lastname")) ? isLinkValid : false;
            _model.firstname = Session["firstname"] == null ? "" : Session["firstname"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.firstname) || !_model.firstname.Contains("firstname")) ? isLinkValid : false;
            _model.cell = Session["cell"] == null ? "" : Session["cell"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.cell) || !_model.cell.Contains("number")) ? isLinkValid : false;
            _model.email = Session["email"] == null ? "" : Session["email"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.email) || !_model.email.Contains("email")) ? isLinkValid : false;
            _model.dob = Session["dob"] == null ? "" : Session["dob"].ToString();// "07/18/2019"
            isLinkValid = (!string.IsNullOrEmpty(_model.dob)) ? isLinkValid : false;
            _model.gender = Session["gender"] == null ? "" : Session["gender"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.gender)) ? isLinkValid : false;
            _model.state = Session["state"] == null ? "" : Session["state"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.state)) ? isLinkValid : false;
            _model.city = Session["city"] == null ? "" : Session["city"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.city)) ? isLinkValid : false;
            _model.address = Session["address"] == null ? "" : Session["address"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.address)) ? isLinkValid : false;
            _model.zip = Session["zipcode"] == null ? _model.zip : Session["zipcode"].ToString();
            isLinkValid = (!string.IsNullOrEmpty(_model.zip)) ? isLinkValid : false;
            string fbid = Session["fbid"] == null ? "" : Convert.ToString(Session["fbid"]);

            var querystring = "fbid=" + fbid + "&dob=" + _model.dob + "&gender=" + _model.gender + "&zip=" + _model.zip + "&fname=" + _model.firstname.Replace(" ", "") + "&lname=" + _model.lastname.Replace(" ", "") + "&phone=" + _model.cell + "&email=" + _model.email + "&city=" + _model.city + "&state=" + _model.state + "&api_key=kghtyb30sumit56&travl=1" + "&addr=" + _model.address;
            string responseString = "{'ResponseCode':' - 1','ResponseMeaning':'Invalid data','ResponseText':'" + querystring + "'}";
            if (!string.IsNullOrEmpty(_model.cell))
            {
                var response = "https://getultimateoffer.com/V1/ismobile?Number=" + _model.cell.Replace('-', ' ').Replace(" ", "").Substring(0, 6);
                responseString = Get(response);
                _model.cell = _model.cell.Replace('-', ' ').Replace(" ", "");
            }
            string utm_source = "";
            string unid = "";
            string clickid = "";
            if (Session["unid"] != null)
            {
                unid = Convert.ToString(Session["unid"]);
                unid = unid.Replace('-', ' ').Replace(" ", "");
            }
            isLinkValid = (!string.IsNullOrEmpty(unid) || !unid.Contains("number")) ? isLinkValid : false;
            if (Session["utm_source"] != null)
            {
                utm_source = Convert.ToString(Session["utm_source"]);
            }
            isLinkValid = (!string.IsNullOrEmpty(utm_source) || !utm_source.Contains("number")) ? isLinkValid : false;
            if (Session["clickid"] != null)
            {
                clickid = Convert.ToString(Session["clickid"]);
            }
            isLinkValid = (!string.IsNullOrEmpty(clickid) || !clickid.Contains("number")) ? isLinkValid : false;
            if (!string.IsNullOrEmpty(unid))
            {
                if (isunidExist(unid) > 0)
                {
                    responseString = "{'ResponseCode':' - 14','ResponseMeaning':'Duplicate Alert, Lead Is Exist in DB','ResponseText':'Duplicate Leads'}";
                }
            }
            Session["response"] = responseString;
            //string EmailVerifyResponse = "";
            //int Isverified = verifyByOne(ref EmailVerifyResponse, _model.email)==true?1:0;
            string id = new Helper().inserts2swithunid(_model, Request.UserHostAddress, responseString, false, utm_source, clickid, "", fbid, unid, Quality: isLinkValid ? "Link Data" : "Valid");
            Session["formlead"] = id;
            if (responseString.ToLower().Contains("success"))
            {
                var values = new NameValueCollection();
                values["dobday"] = Session["dobday"] == null ? "01" : Session["dobday"].ToString();
                values["dobmonth"] = Session["dobmonth"] == null ? "01" : Session["dobmonth"].ToString();
                values["phone"] = _model.cell;
                values["fname"] = FirstCharToUpper(_model.firstname);
                values["lname"] = FirstCharToUpper(_model.lastname);
                values["email"] = _model.email;
                values["city"] = _model.city;
                values["state"] = _model.state;
                values["zip"] = _model.zip;
                values["subid"] = fbid;
                values["unid"] = unid;
                values["street"] = _model.address;
                values["gender"] = _model.gender;
                values["dobyear"] = Session["dobyear"] == null ? "1990" : Session["dobyear"].ToString();
                values["leadid"] = clickid;
                Post("https://getultimateoffer.com/v1/AddSpecialSMS", values);
            }
        }


        public bool verifyByOne(ref string response, string email = "3056umesh@gmail.com")
        {
            bool isVerified = true;//make tru for test 
            string sURL = "https://apps.emaillistverify.com/api/verifyEmail?secret=fekxvamCIRob6AJeAGEEN&email=" + email;
            WebRequest wrGETURL;
            wrGETURL = WebRequest.Create(sURL);
            WebProxy myProxy = new WebProxy("myproxy", 80);
            myProxy.BypassProxyOnLocal = true;
            wrGETURL.Proxy = WebProxy.GetDefaultProxy();
            Stream objStream;
            objStream = wrGETURL.GetResponse().GetResponseStream();
            StreamReader objReader = new StreamReader(objStream);
            response = "";
            int i = 0;
            while (response != null)
            {
                i++;
                response += objReader.ReadLine();
            }
            return isVerified;
        }



        public int isunidExist(string unid = "")
        {
            int isexist = 0;
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@unid", unid);
                var _count = _dt.executeNonQueryWMessage("checkunid", "", _srtlist).ToString();
                isexist = Convert.ToInt16(_count);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
            return isexist;
        }

        public string Post(string uri, NameValueCollection collections)
        {
            string message = "";
            using (var client = new WebClient())
            {
                var response2 = client.UploadValues(uri, collections);
                message = System.Text.Encoding.Default.GetString(response2);
            }
            return message;
        }
        public string Get(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        public Root IsBadIp(ref string _response, string ip = "", string user_agent = "")
        {
            ip = string.IsNullOrEmpty(ip) ? Request.UserHostAddress : ip;
            user_agent = string.IsNullOrEmpty(user_agent) ? Request.UserAgent : user_agent;
            var url = $"https://ipqualityscore.com/api/json/ip/L8A7fsYYUIrM8iPW19Xq9iIKGhMoXKMe/{ip}?strictness=0&allow_public_access_points=true&user_agent={user_agent}&user_language=en-US&campaignID=VSGFT";
            _response = Get(url);
            var data = JsonConvert.DeserializeObject<Root>(_response);
            return data;
        }

        public string checkip(string ip = "", string user_agent = "")
        {
            ip = string.IsNullOrEmpty(ip) ? Request.UserHostAddress : ip;
            user_agent = string.IsNullOrEmpty(user_agent) ? Request.UserAgent : user_agent;
            var url = $"https://ipqualityscore.com/api/json/ip/L8A7fsYYUIrM8iPW19Xq9iIKGhMoXKMe/{ip}?strictness=0&allow_public_access_points=true&user_agent={user_agent}&user_language=en-US&campaignID=VSGFT";
            var _response = Get(url);
            return _response;
        }
        public void insertpaymentlog(string fname, string lname, string email, string phone, int c)
        {
            SqlHelper _dt = new SqlHelper();
            var ip = Request.UserHostAddress;
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@fname", FirstCharToUpper(fname));
                _srtlist.Add("@lname", FirstCharToUpper(lname));
                _srtlist.Add("@ip", ip);
                _srtlist.Add("@email", email);
                _srtlist.Add("@phone", phone);
                _srtlist.Add("@c", c);
                _dt.executeNonQuery("adducprotectionplanslog", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }

        public void CreateXMLAutoResponder1(string value)
        {
            Global global = new Global();
            string empty = string.Empty;
            try
            {
                string file = Server.MapPath("~/App_Data/Logs/" + DateTime.Now.Ticks.ToString() + ".xml");
                string str = string.Concat("<?xml version='1.0' encoding='utf-8' standalone='yes'?><Autosms><C1>", value, "</C1></Autosms>");
                if (!System.IO.File.Exists(file))
                {
                    System.IO.File.Create(file);
                }
                System.IO.File.WriteAllText(file, str);
            }
            catch (Exception exception)
            {
            }
        }
        public int checkexist(string phone = "")
        {
            int isexist = 0;
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@phone", phone);
                var _count = _dt.executeNonQueryWMessage("checkisexist", "", _srtlist).ToString();
                isexist = Convert.ToInt16(_count);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
            return isexist;
        }
        public void updateurl(int urlid = 0)
        {//urlconnection
            var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["urlconnection"].ToString());
            cn.Open();
            SqlCommand cmd = new SqlCommand("update tbl_urlscount set viewpage='Y' where id="+ urlid, cn);
            try
            {
                cmd.ExecuteNonQuery(); 
            }
            catch (Exception)
            {
                
            }
            finally
            {
                cn.Close();
                cn.Dispose();
            } 
        }


        public ActionResult Index(string utm_source = "", string utm_medium = "", string leadid = "", string affiliateid = "", int dobyear = 0, int dobday = 0, int dobmonth = 0, string gender = "", string street = "", string unid = "", string subid = "", string firstname = "", string lastname = "", string phone = "", string email = "", string city = "", string state = "", string zipcode = "", int c = 0, int urlIds = 0)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
           | SecurityProtocolType.Tls11
           | SecurityProtocolType.Tls12
           | SecurityProtocolType.Ssl3;
            ViewBag.isshorturl = 0;
            ViewBag.urlId = urlIds;
            formmodal _model = new formmodal();
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                return Redirect(redirectUrl);
            }
            if (!string.IsNullOrWhiteSpace(utm_source))
            {
                Session["utm_source"] = utm_source;
            }
            if (!string.IsNullOrWhiteSpace(leadid))
            {
                Session["clickid"] = leadid;
            }
            if (!string.IsNullOrEmpty(unid))
            {
                unid = unid.Replace('-', ' ').Replace(" ", "");
                Session["unid"] = unid;
                c = isunidExist(unid);
            }
            else
            {
                Session["unid"] = null;
            }
            if (!string.IsNullOrEmpty(phone))
            {
                _model.cell = phone.Replace('-', ' ').Replace(" ", "");
                Session["cell"] = phone; Session["phone"] = phone;
                c = isunidExist(phone);
            }
            Session["c"] = c == 0 ? "" : c.ToString();
            _model.autoclick = c;
            if (!string.IsNullOrEmpty(firstname))
            {
                ViewBag.isshorturl = 1;
                Session["firstname"] = FirstCharToUpper(firstname);
                _model.firstname = FirstCharToUpper(firstname);
            }
            if (!string.IsNullOrEmpty(lastname))
            {
                Session["lastname"] = FirstCharToUpper(lastname);
                _model.lastname = FirstCharToUpper(lastname);
            }
            if (!string.IsNullOrEmpty(email))
            {
                Session["email"] = email;
                _model.email = email;
            }
            if (!string.IsNullOrEmpty(zipcode))
            {
                Session["zipcode"] = zipcode;
                _model.zip = zipcode;
            }
            if (!string.IsNullOrEmpty(state))
            {
                Session["state"] = state.ToUpper();
                _model.state = state.ToUpper();
            }
            if (!string.IsNullOrEmpty(city))
            {
                Session["city"] = city;
                _model.city = city;
            }
            if (!string.IsNullOrEmpty(subid))
            {
                Session["fbid"] = subid;
            }
            else
            {
                Session["fbid"] = null;
            }
            if (dobmonth != 0)
            {
                Session["dobmonth"] = dobmonth;
                Session["dobday"] = dobday;
                Session["dobyear"] = dobyear;
                Session["dob"] = dobday + "/" + dobmonth + "/" + dobyear;
            }
            if (!string.IsNullOrEmpty(gender))
            {
                Session["gender"] = gender;
                _model.gender = gender;
            }
            else
            {
                Session["gender"] = null;
                _model.gender = "";
            }
            if (!string.IsNullOrEmpty(street))
            {
                Session["address"] = street;
                _model.address = street;
            }
            string view = "index";
            if (Request.Browser.IsMobileDevice == true)
            {
                view = "mobile";
                return View(view, _model);
            }
            else
            {
                return View(view, _model);
            }
        }
    }
}