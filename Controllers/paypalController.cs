﻿using PayPal;
using PayPal.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class paypalController : Controller
    {
        public int insertBusinesslead(formmodal model, string host, int planid, bool IsBusinessProfile = true)
        {
            int id = 0;
            SqlHelper _dt = new SqlHelper();
            var clickid = Session["clickid"] == null ? "" : Session["clickid"].ToString();
            var subid = Session["affiliateid"] == null ? "" : Session["affiliateid"].ToString();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@Address", model.address);
                _srtlist.Add("@City", model.city);
                _srtlist.Add("@clickid", clickid);
                _srtlist.Add("@subid", subid);
                _srtlist.Add("@email", model.email);
                _srtlist.Add("@Fname", model.firstname);
                _srtlist.Add("@Lname", model.lastname);
                _srtlist.Add("@MobileNumber", model.cell);
                _srtlist.Add("@State", model.state);
                _srtlist.Add("@ZipCode", model.zip);
                _srtlist.Add("@isbusinessprofile", IsBusinessProfile);
                _srtlist.Add("@ip", host);
                _srtlist.Add("@planid", 0);
                _srtlist.Add("@businessemail", model.email);
                _srtlist.Add("@businessname", model.firstname + " " + model.lastname);
                _srtlist.Add("@question1", "0");
                _srtlist.Add("@question2", "N");
                _srtlist.Add("@question3", "N");
                _srtlist.Add("@question4", "N");
                id = Convert.ToInt32(_dt.executeNonQueryWMessage("AddspecialbsinessLead", "", _srtlist));
                return id;
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                return 0;
            }
            finally
            {
                _dt.Dispose();
            }
        }

        public bool getallcards(string card)
        {
            SqlHelper _sql = new SqlHelper();
            var crypr = new Crypto();
            bool _stringlist = false;
            string status = "";
            string number = "";
            try
            {
                var _dt = _sql.fillDataTable("getallcards", "", null);
                for (int i = 0; i < _dt.Rows.Count; i++)
                {
                    try
                    {
                        number = crypr.DecryptStringAES(_dt.Rows[i][0].ToString());
                        status = _dt.Rows[i][1].ToString();
                        if (number == card && (status.ToLower() == "order" || status.ToLower() == "refunded"))
                        {
                            _stringlist = true;
                            break;
                        }
                    }
                    catch
                    {

                    }

                }
                _dt.Dispose();
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {

            }
            return _stringlist;
        }

        public bool getallip(string ip)
        {
            SqlHelper _sql = new SqlHelper();
            var crypr = new Crypto();
            bool _stringlist = false;
            try
            {
                SortedList _slist = new SortedList();
                _slist.Add("@ip", ip);
                var _dt = _sql.executeNonQueryWMessage("GetAllIp", "", _slist).ToString();
                _stringlist = Convert.ToInt32(_dt) > 0 ? true : false;
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {

            }
            return _stringlist;
        }

        public bool getallmobile(string mobile)
        {
            SqlHelper _sql = new SqlHelper();
            var crypr = new Crypto();
            bool _stringlist = false;
            try
            {
                SortedList _slist = new SortedList();
                _slist.Add("@ip", mobile);
                var _dt = _sql.executeNonQueryWMessage("GetAllmobile", "", _slist).ToString();
                _stringlist = Convert.ToInt32(_dt) > 0 ? true : false;
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {

            }
            return _stringlist;
        }

        public bool getallemail(string email)
        {
            SqlHelper _sql = new SqlHelper();
            var crypr = new Crypto();
            bool _stringlist = false;
            try
            {
                SortedList _slist = new SortedList();
                _slist.Add("@email", email);
                var _dt = _sql.executeNonQueryWMessage("GetAllemail", "", _slist).ToString();
                _stringlist = Convert.ToInt32(_dt) > 0 ? true : false;
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {

            }
            return _stringlist;
        }


        public void updatelead(int userid, int expirymonth, int expiryday, string cvv, string cardnumber, string cardtype, formmodal model, int planid)
        {
            SqlHelper _dt = new SqlHelper();
            var crypr = new Crypto();
            try
            {
                SortedList _srtlist = new SortedList();
                string username = model.email;
                string password = !string.IsNullOrEmpty(model.cell) ? crypr.EncryptStringAES(model.cell) : "";
                _srtlist.Add("@userid", userid);
                _srtlist.Add("@cvv", crypr.EncryptStringAES(cvv.ToString()));
                _srtlist.Add("@Cardnumber", crypr.EncryptStringAES(cardnumber.ToString()));
                _srtlist.Add("@Cardnumber2", cardnumber.ToString());
                _srtlist.Add("@Cardnumber4", cardnumber.ToString().Substring(cardnumber.ToString().Length - 4, 4));
                _srtlist.Add("@Expirymonth", expirymonth);
                _srtlist.Add("@ExpiryYear", expiryday);
                _srtlist.Add("@CardType", crypr.EncryptStringAES(cardtype.ToString()));
                _srtlist.Add("@city", model.city);
                _srtlist.Add("@state", model.state);
                _srtlist.Add("@zip", model.zip);
                _srtlist.Add("@address", model.address);
                _srtlist.Add("@planid", planid);
                _srtlist.Add("@username", username);
                _srtlist.Add("@password", password);
                _dt.executeNonQuery("updateuserwithcardDetails", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }



        [HttpPost]
        public ActionResult pay(formmodal _model, string nonace, int exdate, int exyear, string ptype)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string mobile = Session["cell"] != null ? Session["cell"].ToString() : "";
           // var context = Configuration.GetAPIContext();
            string email = Session["email"].ToString();
            string host = Request.UserHostAddress;
            int userid = 0;
            int planid = 6;
            string subid = Session["affiliateid"] == null ? "" : Session["affiliateid"].ToString();
            Session["ipavailable"] = false;
            Session["available"] = false;
            if (Session["id"] == null)
            {
                _model.firstname = Session["firstname"].ToString();
                _model.lastname = Session["lastname"].ToString();
                _model.cell = mobile;
                _model.email = email;
                userid = insertBusinesslead(_model, host, 6);
                Session["id"] = userid;
            }
            else
            {
                userid = Convert.ToInt32(Session["id"].ToString());
            }
            _model.firstname = Session["firstname"] == null ? "" : Session["firstname"].ToString();
            _model.lastname = Session["lastname"] == null ? "" : Session["lastname"].ToString();
           // Task.Factory.StartNew(() => updatelead(userid, exdate, exyear, _model.cvv, _model.cardnumber, ptype, _model, planid));
            string price = _model.email == "mjohnson@roguecsc.com" ? "1" : Session["price"] == null ? "19.99" : Session["price"].ToString().Replace("$","");
            Session["ordercount"] = 1;
            Session["price"] = price;

            Session["number"] = _model.cardnumber;
            Session["number1"] = _model.cardnumber;
            Session["cvv"] = _model.cvv;
            Session["exdate"] = exdate;
            Session["exyear"] = exyear;
            Session["ptype"] = ptype;
            Session["address"] = _model.address;
            Session["city"] = _model.city;
            Session["zip"] = _model.zip;
            Session["state"] = _model.state;
            try
            {
                if (mobile != "7574018054")
                {
                    //var blockorders = blockallorders();
                    //if (blockorders || subidblock)
                    //{
                    //    string message = blockorders ? "Blocked - All Orders " : subidblock ? "Blocked order with " + subid : "Block All orders";
                    //    TempData["error"] = "Error : " + message;
                    //    TempData["orderblock"] = "full block";
                    //    insertpaymentlog(userid, Convert.ToDouble(price), planid, true, 30, "Blocked", message);
                    //    // updateleadblocked(cardnumber: _model.cardnumber, phone: mobile);
                    //    return RedirectToAction("index", "declinse");
                    //}
                    //if (isavailable || ismobileavailable || isemailavailable)
                    //{
                    //    TempData["error"] = "Error :Blocked -" + (isavailable ? "Card Block" : isemailavailable ? " Email Block" : "Moblie Block");
                    //    string message = isavailable ? "Blocked - Card already used, card Block xxxx-xxxx-xxxx-" + _model.cardnumber.GetLast(4) : isemailavailable ? "Blocked - Email already used, mobile Block -" + email : "Blocked - Mobile already used, mobile Block -" + mobile;
                    //    insertpaymentlog(userid, Convert.ToDouble(price), planid, true, 30, "Blocked", message);
                    //    updateleadblocked(cardnumber: _model.cardnumber, phone: mobile);
                    //    return RedirectToAction("index", "declinse");
                    //}
                    //var ipexist1 = GetBlockIpList(host);// _dt.tblipblocks.Where(a => a.Ip == host).ToList();
                    //if (ipexist1 > 0)
                    //{
                    //    TempData["error"] = "Error";

                    //    insertpaymentlog(userid, Convert.ToDouble(price), planid, false, 30, "Decline", "Decline - IP Block " + host);
                    //    return RedirectToAction("index", "declinse");
                    //}
                    //var ipexist = GetBlockCardList(_model.cardnumber, mobile); //_dt.tblcardblocks.Where(a => a.card == _model.cardnumber).ToList();
                    //if (ipexist > 0)
                    //{
                    //    TempData["error"] = "Error :Decline - Card Block";
                    //    insertpaymentlog(userid, Convert.ToDouble(price), planid, true, 30, "Decline", "Decline - card Block xxxx-xxxx-xxxx-" + _model.cardnumber.GetLast(4));
                    //    return RedirectToAction("index", "declinse");
                    //}
                }
                //var result = new travelrewardsNew.database.Square().ExecutePayment(Convert.ToDouble(price), nonace);
                //if (result.ToUpper() == "COMPLETED" || result.ToLower() == "approved")
                //{
                //    string log = "Payment done Succesfully-- Paypal Status--- " + result;
                //    insertpaymentlog(userid, Convert.ToDouble(price), planid, true, 30, "approved", log);
                //    //var executedPayment = payment1.Execute(apiContext, paymentExecution);
                //    Session["number"] = null;
                //    Session["cvv"] = null;
                //    Session["exdate"] = null;
                //    Session["exyear"] = null;
                //    Session["ptype"] = null;
                //    Session["address"] = null;
                //    Session["city"] = null;
                //    Session["zip"] = null;
                //    Session["state"] = null;
                //    return RedirectToAction("index", "thanks_you");
                //}
                //else
                //{
                //    TempData["error"] = "";
                //    insertpaymentlog(userid, Convert.ToDouble(price), planid, true, 30, "failed", result);
                //    return RedirectToAction("index", "declinse");
                //}
                return RedirectToAction("index", "declinse");
            }
            catch (PayPalException payPalException)
            {
                string createText = ((PayPal.ConnectionException)payPalException).Response + "---" + payPalException.Message;
                insertpaymentlog(userid, Convert.ToDouble(price), planid, true, 30, "failed", createText);
                return RedirectToAction("index", "declinse");
            }
        }

        public void updateleadblocked(string status = "Blocked", string cardnumber = "", string phone = "")
        {
            int lead = Convert.ToInt32(Session["id"]);
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@phone", phone);
                _srtlist.Add("@status", status);
                _srtlist.Add("@userId", lead); _srtlist.Add("@cardnumber", cardnumber);
                _dt.executeNonQuery("UpdateStatus", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }

        public int GetBlockIpList(string ip)
        {
            int ipcount = 0;
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@ip", ip);
                var database = _dt.fillDataTable("GetBlockIp", "", _srtlist);
                ipcount = Convert.ToInt32(database.Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                return 0;
            }
            finally
            {
                _dt.Dispose();
            }
            return ipcount;
        }

        public int GetBlocksubidList(string subid)
        {
            int ipcount = 0;
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@subid", subid);
                var database = _dt.fillDataTable("GetBlockedSubId", "", _srtlist);
                ipcount = Convert.ToInt32(database.Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                return 0;
            }
            finally
            {
                _dt.Dispose();
            }
            return ipcount;
        }

        public int GetBlocksubidtitleList(string subid)
        {
            int ipcount = 0;
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@subid", subid);
                var database = _dt.fillDataTable("GetBlockedSubIdTitle", "", _srtlist);
                ipcount = Convert.ToInt32(database.Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                return 0;
            }
            finally
            {
                _dt.Dispose();
            }
            return ipcount;
        }

        public bool blockallorders()
        {
            bool isblock = false;
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                var database = _dt.fillDataTable("getblockordercheck", "", null);
                isblock = Convert.ToBoolean(database.Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                var a = ex.Message;

            }
            finally
            {
                _dt.Dispose();
            }
            return isblock;
        }

        public int GetBlockCardList(string ip, string mobile)
        {
            int ipcount = 0;
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@ip", ip);
                _srtlist.Add("@mobile", mobile);
                var database = _dt.fillDataTable("GetBlockcard", "", _srtlist);
                ipcount = Convert.ToInt32(database.Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                return 0;
            }
            finally
            {
                _dt.Dispose();
            }
            return ipcount;
        }
        public void insertpaymentlog(int userId, double amount, int planid, bool issetupfee, int billingdays, string status, string paymentlog)
        {
            SqlHelper _dt = new SqlHelper();
            var clickid = Session["clickid"] == null ? "" : Session["clickid"].ToString();
            var subid = Session["affiliateid"] == null ? "" : Session["affiliateid"].ToString();
            var ip = Request.UserHostAddress;
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@clickid", clickid);
                _srtlist.Add("@subid", subid);
                _srtlist.Add("@ip", ip);

                _srtlist.Add("@userId", userId);
                _srtlist.Add("@Planid", planid);
                _srtlist.Add("@issetupfee", issetupfee);

                _srtlist.Add("@amount", amount);
                _srtlist.Add("@billingdays", billingdays);
                _srtlist.Add("@status", status);
                _srtlist.Add("@paymentlog", amount.ToString() + "$ " + paymentlog);
                _dt.executeNonQuery("addbilling", "", _srtlist);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
        }


    }
}