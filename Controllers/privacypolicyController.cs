﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class privacypolicyController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.url = "https://travelrewardsvacations.com/special#section1";
            if (Request.Url.ToString().Contains("www"))
            {
                string redirectUrl = Request.Url.ToString().Replace("https://www.", "https://");
                Response.Clear();
                Response.StatusCode = 301;
                Response.StatusDescription = "Moved Permanently";
                Response.AddHeader("Location", redirectUrl);
                //    return Redirect(redirectUrl);
            }
            return View("privacy");
        }
    }
}