﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class SqlHelperold : ConnectionHelperold, IDisposable
    {
        private DataSet ds;

        private DataTable dt;

        private SqlDataAdapter adapter = new SqlDataAdapter();

        private SqlCommand com = new SqlCommand();

        public void Dispose()
        {
            if (this.ds != null)
            {
                this.ds.Dispose();
            }
            if (this.dt != null)
            {
                this.dt.Dispose();
            }
            if (this.com != null)
            {
                this.com.Dispose();
            }
            if (this.adapter != null)
            {
                this.adapter.Dispose();
            }
            this.con.Dispose();
            GC.SuppressFinalize(this);
        }

        public void executeNonQuery(string procName, string qry, SortedList list)
        {
            this.com = this.returnCommand(procName, qry, list);
            this.com.ExecuteNonQuery();
            this.con.Close();
        }

        public string executeNonQueryWithTran(string procName, SortedList list, SqlConnection conTran, SqlTransaction tran)
        {
            this.com = this.returnCommand(procName, "", list);
            this.com.Parameters.Add("@xxx", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            this.com.Connection.Close();
            this.com.Connection = conTran;
            this.com.Transaction = tran;
            this.com.ExecuteNonQuery();
            string str = this.com.Parameters["@xxx"].Value.ToString();
            this.con.Close();
            return str;
        }

        public object executeNonQueryWMessage(string procName, string qry, SortedList list)
        {
            this.com = this.returnCommand(procName, qry, list);
            SqlParameter sqlParameter = new SqlParameter("@Mes", SqlDbType.VarChar, 8000)
            {
                Direction = ParameterDirection.Output
            };
            this.com.Parameters.Add(sqlParameter);
            this.com.ExecuteNonQuery();
            this.con.Close();
            return sqlParameter.Value;
        }

        public string executeNonQueryWTranOutMes(string procName, SortedList list, SqlConnection conTran, SqlTransaction tran)
        {
            this.com = this.returnCommand(procName, "", list);
            SqlParameter sqlParameter = new SqlParameter("@Mes", SqlDbType.VarChar, 500)
            {
                Direction = ParameterDirection.Output
            };
            this.com.Parameters.Add(sqlParameter);
            this.com.Connection.Close();
            this.com.Connection = conTran;
            this.com.Transaction = tran;
            this.com.ExecuteNonQuery();
            this.con.Close();
            return sqlParameter.Value.ToString();
        }

        public SqlDataReader executeReader(string procName, string qry, SortedList list)
        {
            this.com = this.returnCommand(procName, qry, list);
            return this.com.ExecuteReader();
        }

        public object executeScaler(string qry)
        {
            if (this.con.State == ConnectionState.Closed)
            {
                this.con.Open();
            }
            this.com = new SqlCommand(qry, this.con)
            {
                CommandType = CommandType.Text
            };
            object obj = this.com.ExecuteScalar();
            this.con.Close();
            return obj;
        }

        public DataSet fillDataSet(string procName, string qry, SortedList list)
        {
            this.com = this.returnCommand(procName, qry, list);
            this.adapter = new SqlDataAdapter(this.com);
            this.ds = new DataSet();
            this.adapter.Fill(this.ds);
            this.con.Close();
            return this.ds;
        }

        public DataTable fillDataTable(string procName, string qry, SortedList list)
        {
            this.com = this.returnCommand(procName, qry, list);
            this.adapter = new SqlDataAdapter(this.com);
            this.dt = new DataTable();
            this.adapter.Fill(this.dt);
            this.con.Close();
            return this.dt;
        }

        public SqlCommand returnCommand(string procName, string qry, SortedList list)
        {
            if (this.con.State == ConnectionState.Closed)
            {
                this.con.Open();
            }
            if (procName != "" && procName != null)
            {
                this.com = new SqlCommand(procName, this.con)
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = 0
                };
            }
            if (qry != "" && qry != null)
            {
                this.com = new SqlCommand(qry, this.con)
                {
                    CommandType = CommandType.Text
                };
            }
            string str = "";
            object value;
            if (list != null)
            {
                foreach (string key in list.Keys)
                {
                    str = (key.Contains("@") ? key : string.Concat("@", key));
                    value = list[key] != null ? list[key].ToString() : "";
                    this.com.Parameters.AddWithValue(str, value);
                }
            }
            return this.com;
        }
    }
}