﻿using System;
using System.Collections;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class thankyou
    {
        public int orderid { get; set; }
        public string revenue { get; set; }
        public string productid { get; set; }
    }
    public class Global
    {
        public SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ToString());
        public const string WebSitePath = "http://vsgft.com";
    }
    public class Helper
    {
        public string inserts2swithunid(formmodal model, string host, string response = "", bool iss2spost = false, string utm_source = "", string clickid = "", string leadresponse = "", string subid = "", string unid = "", string EmailVerifyResponse = "", int IsVerified = 0, string Quality = "")
        {//,string EmailVerifyResponse="",int IsVerified=0
            string mes = "0";
            SqlHelper _dt = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList();
                _srtlist.Add("@EmailVerifyResponse", EmailVerifyResponse);
                _srtlist.Add("@IsVerified", IsVerified);
                _srtlist.Add("@email", model.email);
                _srtlist.Add("@dob", model.dob);
                _srtlist.Add("@Fname", model.firstname.Replace(" ", ""));
                _srtlist.Add("@Lname", model.lastname.Replace(" ", ""));
                _srtlist.Add("@MobileNumber", model.cell);
                _srtlist.Add("@Status", response);
                _srtlist.Add("@leadresponse", leadresponse);
                _srtlist.Add("@ispost", iss2spost);
                _srtlist.Add("@ip", host);
                _srtlist.Add("@utm_source", utm_source);
                _srtlist.Add("@clickid", clickid);
                _srtlist.Add("@subid", subid);
                _srtlist.Add("@unid", unid);
                _srtlist.Add("@address", model.address);
                _srtlist.Add("@city", model.city);
                _srtlist.Add("@state", model.state);
                _srtlist.Add("@zip", model.zip);
                _srtlist.Add("@Quality", Quality);

                mes = _dt.executeNonQueryWMessage("Addnews2slog3", "", _srtlist).ToString();
            }
            catch (Exception ex)
            {
                //StreamWriter sw = new StreamWriter(HttpContext.Current.Server.MapPath("~/App_Data/error.text"));
                ////Write a line of text
                //sw.WriteLine(ex.Message);
                ////Write a second line of text
                //sw.WriteLine(ex.StackTrace);
                ////Close the file
                //sw.Close();
                //var a = ex.Message;
            }
            finally
            {
                _dt.Dispose();
            }
            return mes;
        }

    }
}