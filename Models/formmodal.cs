﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class formmodal
    {
        public int byvisa { get; set; }
        public string fbid { get; set; }
        [RegularExpression("^[a-zA-Z ]+$", ErrorMessage = "Please enter  letters only")]
        [StringLength(30, ErrorMessage = "Please enter at least max {1} and min {2} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "Please enter your full name.")]
        public string fullname { get; set; }
        [RegularExpression("^[a-zA-Z ]+$", ErrorMessage = "Please enter  letters only")]
        [StringLength(30, ErrorMessage = "Please enter at least max {1} and min {2} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "Please enter your first name.", AllowEmptyStrings = false)]
        public string firstname { get; set; }
        [RegularExpression("^[a-zA-Z ]+$", ErrorMessage = "Please enter only  letters and hyphen")]
        [StringLength(30, ErrorMessage = "Please enter at least max {1} and min {2} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "Please enter your last name.", AllowEmptyStrings = false)]
        public string lastname { get; set; }
        [RegularExpression("^[a-zA-Z0-9-#., ]+$", ErrorMessage = "Please enter only alpha/numeric and <br />hyphen, #, or commas.")]
        [StringLength(75, ErrorMessage = "at least max {1} and min {2} characters", MinimumLength = 5)]
        [Required(ErrorMessage = "Please enter your address.", AllowEmptyStrings = false)]
        public string address { get; set; }
        [StringLength(30, ErrorMessage = "Please enter at least max {1} and min {2} characters", MinimumLength = 2)]
        [RegularExpression("^[a-zA-Z ]+$", ErrorMessage = "Please enter only letters or space.")]
        [Required(ErrorMessage = "Please enter your city.", AllowEmptyStrings = false)]
        public string city { get; set; }
        [Required(ErrorMessage = "Please enter your DOB.", AllowEmptyStrings = false)]
        public string dob { get; set; }
        [StringLength(30, ErrorMessage = "Please enter at least max {1} and min {2} characters", MinimumLength = 2)]
        [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "Please enter only letters")]
        [Required(ErrorMessage = "Please enter your state.", AllowEmptyStrings = false)]
        public string state { get; set; }
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter only numbers")]
        [StringLength(5, ErrorMessage = "Please enter at least max {1} and min {2} characters", MinimumLength = 5)]
        [Required(ErrorMessage = "Please enter your zip code", AllowEmptyStrings = false)]
        public string zip { get; set; }
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter only numbers")]
        [StringLength(4, ErrorMessage = "at least max {1} and min {2} characters", MinimumLength = 3)]
        [Required(ErrorMessage = "Please enter your cvv")]
        public string cvv { get; set; }
        [DataType(DataType.CreditCard, ErrorMessage = "Please enter a valid Credit card")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid Credit card")]
        [StringLength(16, ErrorMessage = "Please enter at least max {1} and min {2} characters", MinimumLength = 15)]
        [Required(ErrorMessage = "Please enter a valid Credit card")]
        public string cardnumber { get; set; }
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("^(\\d{10})$", ErrorMessage = "Please enter a valid number")]
        [Required(ErrorMessage = "Please enter a valid number", AllowEmptyStrings = false)]
        public string cell { get; set; }
        [DataType(DataType.EmailAddress, ErrorMessage = "Please Enter Valid Email Address.")]
        [Required(ErrorMessage = "Please Enter a valid Email Address", AllowEmptyStrings = false)]
        public string email { get; set; }
        [Required(ErrorMessage = "Please Enter a Expiry date", AllowEmptyStrings = false)]
        public string expirydate { get; set; }
        public string description { get; set; }
        public string gender { get; set; }
        public int year { get; set; }
        public int month { get; set; }
        public int day { get; set; }
        public string type { get; set; }
        public bool issent { get; set; }
        public string image { get; set; }
        public string message { get; set; }
        public int term { get; set; }
        [RegularExpression("^[a-zA-Z0-9-]+$", ErrorMessage = "Please enter only numbers, letters, or dashes")]
        [StringLength(30, ErrorMessage = "Please enter at least max {1} and min {2} characters", MinimumLength = 5)]
        [Required(ErrorMessage = "Please enter a valid Amazon Order Number")]
        public string amazonorder { get; set; }
        public int autoclick { get; set; }
    }
}