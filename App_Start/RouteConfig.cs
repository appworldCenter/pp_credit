﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication1
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                        name: "step1offer",
                        url: "mobiles/step1offer/{id}",
                        defaults: new { controller = "mobiles", action = "step1offer", id = UrlParameter.Optional }
                    );
            routes.MapRoute(
                       name: "SendnotifyAsync",
                       url: "home/SendnotifyAsync/{id}",
                       defaults: new { controller = "Home", action = "SendnotifyAsync", id = UrlParameter.Optional }
                   );
            routes.MapRoute(
                      name: "SendvisitAsync",
                      url: "home/SendvisitAsync/{id}",
                      defaults: new { controller = "Home", action = "SendvisitAsync", id = UrlParameter.Optional }
                  );
            routes.MapRoute(
                      name: "check_out",
                      url: "check_out/index/{id}",
                      defaults: new { controller = "check_out", action = "index", id = UrlParameter.Optional }
                  );
            routes.MapRoute(
                      name: "privacypolicy",
                      url: "privacypolicy/index/{id}",
                      defaults: new { controller = "privacypolicy", action = "index", id = UrlParameter.Optional }
                  );
            routes.MapRoute(
                     name: "getawayvacation",
                     url: "getawayvacation/index/{id}",
                     defaults: new { controller = "getawayvacation", action = "index", id = UrlParameter.Optional }
                 );
            routes.MapRoute(
                    name: "resortstay",
                    url: "resortstay/index/{id}",
                    defaults: new { controller = "resortstay", action = "index", id = UrlParameter.Optional }
                );
            routes.MapRoute(
                    name: "floridavacation",
                    url: "floridavacation/index/{id}",
                    defaults: new { controller = "floridavacation", action = "index", id = UrlParameter.Optional }
                );
            routes.MapRoute(
                    name: "bahamascruise",
                    url: "bahamascruise/index/{id}",
                    defaults: new { controller = "bahamascruise", action = "index", id = UrlParameter.Optional }
                ); 
            routes.MapRoute(
                     name: "savingsdollars",
                     url: "savingsdollars/index/{id}",
                     defaults: new { controller = "savingsdollars", action = "index", id = UrlParameter.Optional }
                 );
            routes.MapRoute(
                     name: "paypal",
                     url: "paypal/pay/{id}",
                     defaults: new { controller = "paypal", action = "pay", id = UrlParameter.Optional }
                 ); 
            routes.MapRoute(
                     name: "thanks_you",
                     url: "thanks_you/index/{id}",
                     defaults: new { controller = "thanks_you", action = "Index", id = UrlParameter.Optional }
                 );
            routes.MapRoute(
                     name: "declinse",
                     url: "declinse/index/{id}",
                     defaults: new { controller = "declinse", action = "Index", id = UrlParameter.Optional }
                 );
            routes.MapRoute(
                    name: "step1",
                    url: "mobiles/step1/{id}",
                    defaults: new { controller = "mobiles", action = "step1", id = UrlParameter.Optional }
                );
            routes.MapRoute(
                    name: "step2",
                    url: "mobiles/step2/{id}",
                    defaults: new { controller = "mobiles", action = "step2", id = UrlParameter.Optional }
                );
            routes.MapRoute(
                    name: "step3",
                    url: "mobiles/step3/{id}",
                    defaults: new { controller = "mobiles", action = "step3", id = UrlParameter.Optional }
                );
            routes.MapRoute(
             name: "CatchAll",
             url: "{*any}",
             defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
         );
            
        }
    }
}
