﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.utility
{
    public static class helper
    {
        public static string Shuffle(this string str)
        {
            char[] array = str.ToCharArray();
            Random rng = new Random();
            int n = array.Length;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                var value = array[k];
                array[k] = array[n];
                array[n] = value;
            }
            return new string(array);
        }

        public static string GetRandomalphanumeric(string numerics = "123496785", string chars = "ABCDEFGHIJKLghijklmnopqMNOPQRSTUVWXYZabcdefrstuvwxyz")
        {
            int _numbers = 4;
            int _chars = 2;
            char[] chrArray = new char[_chars];
            Random random = new Random();
            for (int i = 0; i < (int)chrArray.Length; i++)
            {
                chrArray[i] = chars[random.Next(chars.Length)];
            }
            char[] chrArray1 = new char[_numbers];
            Random random1 = new Random();
            for (int j = 0; j < (int)chrArray1.Length; j++)
            {
                chrArray1[j] = numerics[random1.Next(numerics.Length)];
            }
            string numbers = string.Concat( new string(chrArray), new string(chrArray1)); 
            return numbers.Shuffle();
        }
        public static string GetRandomalphanumeric2(string numerics = "127893456", string chars = "APQRSTUVWXYZabcdefghijklmnoBCDEFGHIJKLMNOpqrstuvwxyz")
        {
            int _numbers = 5;
            int _chars = 1;
            char[] chrArray = new char[_chars];
            Random random = new Random();
            for (int i = 0; i < (int)chrArray.Length; i++)
            {
                chrArray[i] = chars[random.Next(chars.Length)];
            }
            char[] chrArray1 = new char[_numbers];
            Random random1 = new Random();
            for (int j = 0; j < (int)chrArray1.Length; j++)
            {
                chrArray1[j] = numerics[random1.Next(numerics.Length)];
            }
            string numbers = string.Concat( new string(chrArray1), new string(chrArray));
            return numbers.Shuffle();
        }

    }
}